#coding=gbk 
from flask import Flask
import os
from apscheduler.scheduler import Scheduler
import apscheduler

#create app
app = Flask(__name__)
sched = Scheduler()

#load config file
app.config.from_pyfile('config/flask.ini')

#get hostname,ip,cluster_name
from dbagent.model import main
model.main.load_host_meta()

#check is crontab log directory exists
if not os.path.exists(app.config['CRONLOG_PATH']):
    os.mkdir(app.config['CRONLOG_PATH'])

#create apscheduler
app.config['sched']=sched
from dbagent.model import task
sched.add_listener(task.err_listener, apscheduler.events.EVENT_JOB_ERROR|apscheduler.events.EVENT_JOB_EXECUTED)
task.load_jobs()
sched.start()
app.config['script_error_cnt']=dict()

#load blue prints
from dbagent.view import main,task,route
BLUE_PRINTS=(main,task,route)
for bp in BLUE_PRINTS:
    app.register_blueprint(bp)

import logging
logging.basicConfig()

#load routes
from dbagent.model import route
route.load_and_process_routes()


@app.errorhandler(404)
def not_found(error):
    return "not found this page..", 404 

@app.errorhandler(500)
def server_error(error):
    return "had a problem...", 500 

@app.before_request
def before_request():
    pass

@app.teardown_request
def teardown_request(exception):
    pass
