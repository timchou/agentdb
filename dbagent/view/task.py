#coding=gbk
from flask import Blueprint ,jsonify,request,url_for,redirect,render_template 
from dbagent import app,model,sched
from dbagent.config.pmysql import MySQL
import os,datetime
from apscheduler.scheduler import Scheduler
import apscheduler

task=Blueprint('task' ,__name__,url_prefix='/task')

@task.route('/add_task',methods=['GET','POST'])
def add_task():
    error=None
    if request.method == 'POST':
        data=dict()

        data['interpreter']=model.main.req('interpreter')
        data['uri']=model.main.req('uri')
        data['args']=model.main.req('args')
        data['task_type']=model.main.req('task_type',type='int')
        data['to_log']=model.main.req('to_log')
        data['to_run']=model.main.req('to_run')
        data['to_all']=model.main.req('to_all')

        if not data['uri'] or data['task_type'] not in (1,2,3) or data['to_run'] not in ('y','n') or data['to_log'] not in ('y','n'):
            return redirect(url_for('main.add_task',error=u'��������'))

        if data['task_type'] == 1:
            data['year']=model.main.req('crontab_year')
            data['month']=model.main.req('crontab_month')
            data['day']=model.main.req('crontab_day')
            data['week']=model.main.req('crontab_week')
            data['day_of_week']=model.main.req('crontab_day_of_week')
            data['hour']=model.main.req('crontab_hour')
            data['minute']=model.main.req('crontab_minute')
            data['second']=model.main.req('crontab_second')
        elif data['task_type'] == 2:
            data['date_date']=model.main.req('date_date')
        elif data['task_type'] == 3:
            data['weeks']=model.main.req('intval_weeks')
            data['days']=model.main.req('intval_days')
            data['hours']=model.main.req('intval_hours')
            data['minutes']=model.main.req('intval_minutes')
            data['seconds']=model.main.req('intval_seconds')
            data['start_date']=model.main.req('start_date')

        res=model.task.add_task(data)

    return render_template('add_task.html',**locals())

@task.route('/reload' ,methods=['GET'])
def reload_task():
    global sched
    
    for job in sched.get_jobs():
        if job.name in ('clear_error_cnt',):
            continue
        sched.unschedule_job(job)

    model.task.load_jobs()

    return jsonify(res='ok') 

@task.route('/list' ,methods=['GET'])
def list_task():
    jobs=sched.get_jobs()
    if not jobs:
        return 'no jobs'
    
    j=list()
    for job in jobs:
        data={
            'func':job.name,
            'trigger':str(job.trigger),
            'runs':job.runs,
            'instances':job.instances,
            'next_fire_time':str(job.trigger.get_next_fire_time(datetime.datetime.now()))
            }
        if job.args:
            data['interpreter']=job.args[0]['interpreter']
            data['args']=job.args[0]['uri']
            data['to_log']=job.args[0]['to_log']
            data['to_all']=job.args[0]['to_all']
            if 'last_run_time' in job.args[0]:
                data['last_run_time']=str(job.args[0]['last_run_time'])
            else:
                data['last_run_time']='not run yet'
            if 'logfile' in job.args[0]:
                data['logfile']=job.args[0]['logfile']
        j.append(data)

    return jsonify(cnt=len(j),jobs=j) 
