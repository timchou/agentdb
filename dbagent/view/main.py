#coding=gbk
from flask import Blueprint,jsonify,request,render_template
import subprocess,os,signal
from dbagent import model,app

main=Blueprint('main' ,__name__)


@main.route('/' ,methods=['GET'])
def index():
    return 'hello' 

@main.route('/orzdba' ,methods=['GET'])
def orzdba():
    mysqlport=request.args.get('port','3306')
    port=int(mysqlport)

    filename='/tmp/orzdba_data/' + model.main.get_random_name() 
    cmd="export PATH=$PATH:/home/oracle/mysql/bin && export MYSQL_HOME=/data/mysql%d && . /home/oracle/bin/dbrole.sh && [ -d /tmp/orzdba_data ] || mkdir /tmp/orzdba_data && touch %s && /home/oracle/bin/orzdba -lazy -L %s" \
        % (port,filename,filename)

    p=subprocess.Popen(cmd,shell=True,preexec_fn=os.setpgrp)
    pid=p.pid
    
    #curr_pos,lines=model.main.get_file_content(filename,0)

    return render_template('orzdba.html',**locals()) 


@main.route('/get_file_content' ,methods=['GET','POST'])
def get_file_content():
    pos=model.main.req('pos',0)
    pos=int(pos)

    filename=model.main.req('filename')
    if not filename:
        return jsonify(res=False, message='filename error' )

    curr_pos,content=model.main.get_file_content(filename,pos)
    if curr_pos is False or content is False:
        return jsonify(res=False, message='filename error' )

    return jsonify(res=True, content=content, pos=curr_pos )

@main.route('/orzdba_kill' ,methods=['GET','POST'])
def orzdba_kill():
    pid=model.main.req('pid')
    if not pid:
        return jsonify(res=False,message='no pid')

    pid=int(pid)
    p = subprocess.Popen(['ps', str(pid)], stdout=subprocess.PIPE)
    out, err = p.communicate()
    lines=list()
    for line in out.splitlines():
        line=line.strip()
        lines.append(line)

    if len(lines) != 2 or 'orzdba' not in lines[1]:
        return jsonify(res=False,message='not match')
    
    pgid=os.getpgid(pid)
    os.killpg(pgid, 9)
    return jsonify(res=True)
