#coding=gbk
from flask import Blueprint ,jsonify,request,url_for,redirect,render_template 
from dbagent import app,model
import os

route=Blueprint('route' ,__name__,url_prefix='/route')

@route.route('/execute',methods=['GET','POST'])
def execute():
    print 'conn from ',request.remote_addr
    print 'args:',request.args

    cmdid=model.main.req('cmdid',None,'int')
    arguments=model.main.req('args',None)
    if not cmdid:
        return jsonify(res=False)

    logfile,out,err=model.route.do_job(cmdid=cmdid,arguments=arguments)
    return jsonify(res=True,logfile=logfile,out=out,err=err)

@route.route('/list',methods=['GET',])
def get_routes_list():
    if 'routes' in app.config and app.config['routes']:
        newl=list()
        for r in app.config['routes'].keys():
            tmp=app.config['routes'][r]
            tmp['gmt_created']=str(tmp['gmt_created'])
            newl.append(tmp)

    return jsonify(routes=newl)

@route.route('/reload',methods=['GET',])
def reload_routes():
    model.route.load_and_process_routes()

    return jsonify(res='ok')
