#coding=gbk
import MySQLdb,_mysql_exceptions
import sys,socket,random,os,time
from numbers import Number

HOSTS=[
    {'ip':'10.228.86.27',
     'port':3306
    },
    {'ip':'askdba.alipay.net',
     'port':3316
    },
]

USER='monitor'
PASS='monitor'
DB='tianji'

HOST=None
PORT=None
timeout = 1
for host in HOSTS:
    try:
        socket.gethostbyaddr(host['ip'])
        HOST=host['ip']
        PORT=host['port']
        break
    except socket.herror:
        continue

class MySQL:
    def __init__(self,host=HOST,user=USER,password=PASS,database=DB,port=PORT,ret=False):

        self.host=host
        self.user=user
        self.password=password
        self.database=database
        self.port=port
        
        reconn_cnt=0
        while True:
            try:
                self.conn = MySQLdb.connect(host=self.host, user=self.user, passwd=self.password, db=self.database,port=port,charset="utf8",connect_timeout=5)
                break
            except _mysql_exceptions.OperationalError:
                if ret:
                    raise 

                random.seed(os.getpid()+time.time())
                st=random.random()*5
                print 'cannot connect to MySQL:%s:%d,%s,%s,CNT:%d,sleep for %f seconds' %(self.host,int(self.port),self.user,self.password,reconn_cnt,st)
                reconn_cnt+=1
                if reconn_cnt >= 5:
                    exit(1)

                time.sleep(st)
 
        self.conn.autocommit(1)
        self.cursor = self.conn.cursor(MySQLdb.cursors.DictCursor)
    
    def escape(self,content):
        return MySQLdb.escape_string(content.encode('utf-8')).decode('utf-8')

    def the_all(self,query):
        self.cursor.execute(query.encode('utf-8'))
        return self.cursor.fetchall()

    def the_one(self,query):
        self.cursor.execute(query.encode('utf-8'))
        return self.cursor.fetchone()

    def execute(self,query):
        self.cursor.execute(query.encode('utf-8'))
        return True

    def insert(self,tbl,doc,UPDATE=False):
        for i in doc.keys():
            if doc[i] is None:
                del(doc[i])

        def addp(value):
            return '`'+value+'`'

        def update(value):
            return "`%s`=VALUES(`%s`)" % (value,value)

        def packData(value):
            if isinstance(value,str) or isinstance(value,unicode):
                value=self.escape(value)
                return '"' + value + '"'
            elif isinstance(value,int):
                return str(value)
            elif isinstance(value,Number):
                return str(value)

        cols=doc.keys()
        vals=doc.values()

        query="insert into %s (%s) values(%s) " % (tbl, ",".join(map(addp,cols)) , ",".join(map(packData,vals)))
        if UPDATE:
            if 'gmt_created' in cols:
                cols.remove('gmt_created')

            u=" ON DUPLICATE KEY UPDATE %s " % ",".join(map(update,cols))
            query =query + u

        try:
            self.cursor.execute(query.encode('gbk'))
        except _mysql_exceptions.IntegrityError:
            print 'dump...'
            pass
        return self.cursor.lastrowid

    def update(self,tbl,pk,doc):
        if not tbl or not pk or not doc:
            return False

        sqls=list()
        for key in doc.keys():
            if key == pk:
                continue
            if isinstance(doc[key],int):
                sqls.append(" %s=%d " % (key,int(doc[key])) )
            if isinstance(doc[key],float):
                sqls.append(" %s=%f " % (key,float(doc[key])) )
            if isinstance(doc[key],basestring):
                sqls.append(" %s='%s' " % (key,doc[key]) )
        if sqls:
            sql="UPDATE %s SET %s WHERE %s=%d" % ( tbl, ",".join(sqls) , pk ,int(doc[pk]))
            return self.cursor.execute(sql)

        return False

    def get_host_meta(self,host_name=None):
        if not host_name:
            return

        sql="select * from meta_host where host_name='%s' limit 1" % host_name
        return self.the_one(sql)
