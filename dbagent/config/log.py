#coding=gbk
from dbagent import app
import logging
from logging.handlers import RotatingFileHandler
from logging import Formatter


filename='/tmp/agentdb.log'
handler = RotatingFileHandler(filename, maxBytes=10000, backupCount=1)
handler.setLevel(logging.INFO)
handler.setFormatter( Formatter('%(asctime)s [in %(pathname)s:%(lineno)d] %(levelname)s: %(message)s') )
app.logger.addHandler(handler)
