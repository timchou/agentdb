var i = 1;

function addslashes( str ) {
	return (str+'').replace(/([\\"'])/g, "\\$1").replace(/\0/g, "\\0");
}

function request_data(){
	var filename= $('#filename').val();
	var pos= $('#pos').val();

	$.ajax({
		type    :   "POST",
		url     :   "/get_file_content",
		data    :   {filename:filename,pos:pos}
	}).done(function( val) {
		if(val['res'] == true){
			if(val['content'].length == 0){
				i++;
			}
			$('#content').append( addslashes(val['content']) );
			$('#pos').val(val['pos']);
			$(document).scrollTop($(document).height());
		}
	});
}


function myLoop () {
	setTimeout(function () { 
		request_data();
	
		if (i >= 10) { 
			alert('no more data...');
		}else{
			myLoop();
		}
	}, 2500)
}

$(document).ready(function() {	

	myLoop();
});

$(window).on('beforeunload', function(){
	
	var pid= $('#pid').val();
	$.ajax({
		type    :   "POST",
		url     :   "/orzdba_kill",
		data    :   {pid:pid}
	}).done(function( val) {
	});
	
	return 'Are you sure you want to leave?';
});


