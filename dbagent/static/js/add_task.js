$(document).ready(function() {	
	$('#task_type').change(function() {
		var t=$("#task_type").val();
		if(t == 1){
			$("#t_crontab").removeClass('hide');
			$("#t_date").addClass('hide');
			$("#t_interval").addClass('hide');
		}else if(t == 2){
			$("#t_crontab").addClass('hide');
			$("#t_date").removeClass('hide');
			$("#t_interval").addClass('hide');
		}else if(t == 3){
			$("#t_crontab").addClass('hide');
			$("#t_date").addClass('hide');
			$("#t_interval").removeClass('hide');
		}
	});
});
