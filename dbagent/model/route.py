#coding=gbk
from dbagent import app
from dbagent.config.pmysql import MySQL
from dbagent.model import main as Main
from flask import session,request,url_for
import time,random,subprocess,traceback,os,datetime,time,pwd

def prefun():
    #uid=pwd.getpwnam(app.config['MYSQL_USER']).pw_uid
    #os.setuid(uid)
    pass

def do_job(**args):
    route=None
    cmd=''
    if 'cmdid' in args and args['cmdid']:
        route=app.config['routes'][args['cmdid']]
        cmd=route['cmd']
    else:
        return False

    arguments=None
    if 'arguments' in args and args['arguments']:
        cmd += ' ' + args['arguments']

    if cmd:
        logfile=None
        out=None
        err=None
        if route['to_log'] == 'y':
            #/tmp/agentdb/20130406/
            DIR=app.config['CRONLOG_PATH'] + '/' + datetime.date.today().strftime("%Y%m%d") + '/'
            if not os.path.exists(DIR):
                os.mkdir(DIR)

            output=logfile= DIR + 'cmd_' + str(route['cmdid']) + '.' + str(int(time.time()))

            f=open(output,'w')
        else:
            f=subprocess.PIPE

        print 'now execute:',cmd
        p=subprocess.Popen(cmd,shell=True,stdout=f,stderr=f,preexec_fn=prefun)
        if route['to_wait'] == 'y':
            out, err = p.communicate()

        return logfile,out,err

    return False

def get_route_data_from_tianji():
    db=MySQL()
    sql="select * from agentdb_route where to_run='y'"

    rows=db.the_all(sql)
    if not rows:
        return None

    return rows

def load_and_process_routes():
    routes=get_route_data_from_tianji()
    if not routes:
        return 
    
    app.config['routes'] =dict()
    for r in routes:
        app.config['routes'][r['cmdid']]=r
