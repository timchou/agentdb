#coding=gbk
from dbagent import app
from dbagent.config.pmysql import MySQL
from flask import session,request,url_for
import time,random,subprocess,datetime,socket,os

MAX_STR=150

def _time(format='%Y-%m-%d %H:%M:%S'):
    return datetime.datetime.now().strftime(format)

def req(args,default=None,type=None):
    if request.method == 'POST':
        data=request.form.get(args,default)
    else:
        data=request.args.get(args,default)

    if data:
        if type == 'int':
            data=int(data)
        elif type == 'float':
            data=float(data)

    return data

def get_file_content(filename,pos):
    if not os.path.exists(filename):
        return False,False

    f=open(filename)

    f.seek(pos,0)
    lines=f.readlines()
    curr_pos=f.tell()
    
    content=''
    for line in lines:
        row="<br />".join(line.split("\n"))
        #if len(row) >= MAX_STR:
        #    row="<br />".join( [row[i:i+MAX_STR] for i in range(0, len(row), MAX_STR)])
        content += row

    return curr_pos,content


def get_random_name():
    s1=str(time.time())
    s2=''.join(random.sample([chr(i) for i in range(97, 123)], 6))

    return s1+s2

def load_host_meta():
    f = open('/proc/sys/kernel/hostname', 'r')
    app.config['host_name']=f.readline().rstrip()
    app.config['ip']=socket.gethostbyname(app.config['host_name'])
    app.config['cluster']=dict()

    db=MySQL()
    sql="select instance_name,cluster from db_list where ip='%s'" % app.config['ip']
    rows=db.the_all(sql)
    if rows:
        for row in rows:
            _,port=row['instance_name'].split(':')
            app.config['cluster'][int(port)]=row['cluster']

    print 'host_name:',app.config['host_name']
    print 'ip:',app.config['ip']
    print 'cluster:',app.config['cluster']
