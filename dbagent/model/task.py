#coding=gbk
from dbagent import app,sched
from dbagent.config.pmysql import MySQL
from dbagent.model import main as Main
from flask import session,request,url_for
import time,random,subprocess,traceback,os,datetime,time


def add_task(data):
    if not data:
        return False
    
    data['gmt_created']=Main._time()

    if data['task_type'] == 1:
        if data['year'] == data['month'] == data['day'] == data['day_of_week'] == data['hour'] == data['minute'] == data['second'] == '*':
            return False

    db=MySQL()
    return db.insert('db_task',data)

def get_task_data_from_tianji():
    db=MySQL()
    sql="select * from db_task where to_run='y' and to_all='y'"

    rows=db.the_all(sql)
    if not rows:
        return None

    return rows

def get_spec_task_data_from_tianji():
    def addp(s):
        return '"' + s + '"'

    db=MySQL()
    sql="select * from db_task_spec where ip='%s' or host_name='%s'" % (app.config['ip'],app.config['host_name'])
    
    for key in list(app.config['cluster'].keys()):
        if not app.config['cluster'][key]:
            del app.config['cluster'][key]

    if app.config['cluster']:
        sql += " or cluster in (%s) " % (",".join(map(addp,app.config['cluster'].values())))

    rows=db.the_all(sql)
    if not rows:
        return None

    db_task_ids=list()
    for row in rows:
        db_task_ids.append(row['db_task_id'])

    if db_task_ids:
        sql="select * from db_task where id in (%s)" % ",".join(map(str,db_task_ids))
        rows=db.the_all(sql)
        return rows

    return None

def err_listener(ev):
    '''
    event attached to every JOB
    after job executed,we get its return value,see if is has done successfully
    '''
    #if not ev.job.args:
    #    ev.job.args=list()
    #    ev.job.args.append(dict())

    if ev.job.name not in ('do_job',):
        app.logger.info("cron inner job %s successfully" % ev.job.name)
        return 

    trigger=ev.job.trigger
    args=ev.job.args
    retval=ev.retval
    exp=str(ev.exception)
    tb=str(traceback.extract_tb(ev.traceback))
    
    ev.job.args[0]['last_run_time']=datetime.datetime.now()
    
    '''when we had problem with job running,we record info to tianji,
    '''
    if  retval != 0:
        '''calculate EVERY script's error time in 1 HOUR
        if the count >= 5 ,we stop running this script anymore'''
        if args[0]['uri'] in app.config['script_error_cnt']:
            if app.config['script_error_cnt'][args[0]['uri']] >= 5:
                app.logger.error("%s has been remove because it's error_cnt >= 5" % args[0]['uri'])
                sched.unschedule_job(ev.job)
                return
        else:
            app.config['script_error_cnt'][args[0]['uri']] = 0

        app.config['script_error_cnt'][args[0]['uri']] += 1

        data={
            'host_name' :app.config['host_name'],
            'ip'        :app.config['ip'],
            'uri'       :args[0]['uri'], 
            'cmd'       :args[0]['cmd'],
            'logfile'   :args[0]['logfile'],
            'task_type' :args[0]['task_type'],
            'cron'      :str(trigger),
            'retval'    :str(retval),
            'start_time':args[0]['start_time'],
            'gmt_created':Main._time()
            }
        db=MySQL()
        db.insert('db_task_log',data)
        #app.logger.error('job return error retval:,logfile:%s' % (args,retval,exp,tb,args[0]['logfile']) )
    else:
        app.logger.info("cron external job %s successfully" % args[0]['cmd'])
        


@sched.interval_schedule(hours=1)
def clear_error_cnt():
    app.config['script_error_cnt']=dict()

def do_job(task):
    '''
    do not use output at this moment

    output file:
        /tmp/agentdb/20130406/scriptname.timestamp
    '''
    
    cmd=''
    if task['interpreter']: 
        cmd += task['interpreter'] + ' '
        
    if task['uri']:
        cmd += task['uri'] + ' '
        
    if task['args']:
        cmd += task['args']
        
    task['cmd']=cmd
    scriptname=os.path.basename(task['uri'])

    if cmd and scriptname:
        if task['to_log'] == 'y':
            #/tmp/agentdb/20130406/
            DIR=app.config['CRONLOG_PATH'] + '/' + datetime.date.today().strftime("%Y%m%d") + '/'
            if not os.path.exists(DIR):
                os.mkdir(DIR)

            output= DIR + scriptname + '.' + str(int(time.time()))
            task['logfile']=output

            f=open(output,'w')
        else:
            f=subprocess.PIPE

        task['start_time']=Main._time()
        p=subprocess.Popen(cmd,shell=True,stdout=f,stderr=f)
        ret_code = p.wait()
        return ret_code


def load_jobs():
    tasks=get_task_data_from_tianji()
    if not tasks:
        app.logger.warning("no comm task")
        return
    else:
        app.logger.info("got %d comm tasks" % len(tasks))

    spec_tasks=get_spec_task_data_from_tianji()
    if not spec_tasks:
        app.logger.warning("no spec task")
    else:
        app.logger.info("got %d spec tasks" % len(spec_tasks))
    
    if spec_tasks:
        tasks=tasks + spec_tasks

    print 'total:',len(tasks)

    for task in tasks:
        try:
            with open(task['uri']): pass
        except IOError:
            app.logger.warning("task file not exist:%s" % task['uri'])
            continue

        if task['task_type'] == 1:
            for key in task.keys():
                if key in ('year','month','day','week','day_of_week','hour','minute','second') and not task[key]:
                    task[key]=None

            sched.add_cron_job(func=do_job,\
                                year=task['year'],\
                                month=task['month'],\
                                day=task['day'],\
                                week=task['week'],\
                                day_of_week=task['day_of_week'],\
                                hour=task['hour'],\
                                minute=task['minute'],\
                                second=task['second'],\
                                args=[task])

        elif task['task_type'] == 2:
            sched.add_date_job(func=do_job,date=task['date'],args=[task])

        elif task['task_type'] == 3:
            sched.add_interval_job(func=do_job,\
                                    weeks=task['weeks'],\
                                    days=task['days'],\
                                    hours=task['hours'],\
                                    minutes=task['minutes'],\
                                    seconds=task['seconds'],\
                                    start_date=task['start_date'],\
                                    args=[task])
