#!/home/oracle/dbaperl/bin/perl
use Time::Local;
#
# Purpose: gather mysql warn info
# rules: threads_running > 20, rt > 100ms, disk, qps > 10000, tps > 10000, slave delay > 100
#
# base line: qps, tps, ins, upd, del, sel, connections, threads_running
#
use DBI;
use Encode;
use Getopt::Long;

###########################################
sub print_usage () {
  my $text = <<EOF;
  
 NAME:
    tbsync

 Example:

 FUNCTION:
    Table structure and table data compare and sync

 PARAMETER:
    most of the parameter is self explained
            only check table structure
        --recheck=n

EOF
 print STDERR $text;
 exit 0;
}

my %opt = (
);

GetOptions(\%opt,
    'd|day=s',           #day 2012-11-11
    'h|help',            # help
    'recheck=i'
) or print_usage();

if ( $opt{h} ) {
    &print_usage();
}

#conn to mysql db using DBI(dbd-mysql)
sub get_mysql_connection {
    my ($mysql_instance, $user, $pass, $db) = @_;
    my ($ip_addr, $port) = split /:/, $mysql_instance;
    my $str_conn = "dbi:mysql:host=$ip_addr;port=$port;db=$db";
    local $SIG{ALRM} = sub { die "Connect to MySQL $ip_addr:$port timed out\n" };
    alarm 20;
    my $mydb = DBI->connect(
        $str_conn,
        "$user", "$pass") or die "Connect to MySQL $ip_addr:$port error:". DBI->errstr;
    $mydb->{FetchHashKeyName} = 'NAME_lc';
    $mydb->{AutoCommit} = 1;
    $mydb->{RaiseError} = 1;
    $mydb->{PrintError} = 1;

    $mydb->do("set names gbk");
    alarm 0;
    return $mydb;
}

sub get_instance_list {
    my ($dbh) = @_;
    my $sql = "select instance_name, host_name, cluster, node from db_list where check_time > date_sub(now(), interval 12 hour)";
    my $stmt = $dbh->prepare($sql);
    $stmt->execute();
    my $rset = $stmt->fetchall_arrayref();
    return $rset;
}

#+-----------------+--------------+------+-----+---------+-------+
#| Field           | Type         | Null | Key | Default | Extra |
#+-----------------+--------------+------+-----+---------+-------+
#| instance_name   | varchar(30)  | NO   | PRI |         |       |
#| hhmm            | varchar(4)   | NO   | PRI |         |       |
#| check_time      | datetime     | YES  |     | NULL    |       |
#| sqps            | int(11)      | YES  |     | NULL    |       |
#| qps             | int(11)      | YES  |     | NULL    |       |
#| tps             | int(11)      | YES  |     | NULL    |       |
#| ins             | int(11)      | YES  |     | NULL    |       |
#| upd             | int(11)      | YES  |     | NULL    |       |
#| del             | int(11)      | YES  |     | NULL    |       |
#| sel             | int(11)      | YES  |     | NULL    |       |
#| connections     | int(11)      | YES  |     | NULL    |       |
#| threads_running | int(11)      | YES  |     | NULL    |       |
#| rt              | int(11)      | YES  |     | NULL    |       |
#| load1           | int(11)      | YES  |     | NULL    |       |
#| cpu_user        | decimal(4,2) | YES  |     | NULL    |       |
#| cpu_wait        | decimal(4,2) | YES  |     | NULL    |       |
#| cpu_sys         | decimal(4,2) | YES  |     | NULL    |       |
#| net_recv        | int(11)      | YES  |     | NULL    |       |
#| net_send        | int(11)      | YES  |     | NULL    |       |
#+-----------------+--------------+------+-----+---------+-------+
my %warn_thres =  (
    "qps" =>      [10000,   1.5,  10],
    "tps" =>      [5000,    1.5,  10],
    "sqps" =>     [10,      1.5,  10],
    "ins"  =>     [3000,    1.5,  10],
    "upd"  =>     [3000,    1.5,  10],
    "del"  =>     [3000,    1.5,  10],
    "sel"  =>     [8000,    1.5,  10],
    "act"  =>     [20,      1.5,  10],
    "rt_avg"  =>  [100000,  1.5,  10],
    "conn"  =>    [6000,    1.5,  10],
    "load1"  =>   [10,      1.5,  10],
    "cpu_usr"  => [10,      1.5,  10],
    "cpu_sys"  => [10,      1.5,  10],
    "cpu_wai"  => [10,      1.5,  10],
    "net_recv"  =>[10,      1.5,  10],
    "net_send"  =>[10,      1.5,  10]
);

#rows_inserted, rows_updated, rows_deleted, rows_selected
#logical reads, physical reads
#other kpi

sub gather_warn_info_1min {
    my ($dbh, $cluster_name, $node_name, $instance_name, $host_name, $start_time, $end_time, $h, $m) = @_;
    $cluster_name = "UNKNOWN" if (!defined($cluster_name));
    $node_name = "UNKNOWN" if (!defined($node_name));
    #print "instance_name, $host_name, $start_time, $end_time, $h, $m\n";
    my $sql_mysqlstat = <<EOD;
        select avg(sqps) sqps, avg(qps) qps, avg(tps) tps, avg(ins) ins, avg(upd) upd, avg(del) del, 
            avg(sel) sel, avg(threads_connected) conn, avg(threads_running) act
        from mysql_stat
        where instance_name = ?
        and check_time >= ?
        and check_time < ?
EOD
    my $sql_mysqlrt = <<EOD;
        select avg(rt_avg) rt_avg
        from rt_tcprstat
        where instance_name = ?
        and check_time >= ?
        and check_time < ?
EOD
    my $sql_hoststat = <<EOD;
        select avg(load1) load1, avg(cpu_usr) cpu_usr, avg(cpu_sys) cpu_sys, avg(cpu_wai) cpu_wai,
            avg(net_recv) net_recv, avg(net_send) net_send
        from linux_stat
        where host_name = ?
        and check_time >= ?
        and check_time < ?
EOD
    my @kpi = qw(sqps qps tps ins upd del sel conn act rt_avg load1 cpu_usr cpu_sys cpu_wai net_recv net_send);

    #mysql_stat
    my %curr_stat;
    my $ii = 0;
    my $stmt = $dbh->prepare($sql_mysqlstat);
    $stmt->execute($instance_name, $start_time, $end_time);
    my @r_mysqlstat = $stmt->fetchrow_array();

    #rt_tcprstat
    $stmt = $dbh->prepare($sql_mysqlrt);
    $stmt->execute($instance_name, $start_time, $end_time);
    my @r_mysqlrt = $stmt->fetchrow_array();

    #linux_stat
    $stmt = $dbh->prepare($sql_hoststat);
    $stmt->execute($host_name, $start_time, $end_time);
    my @r_hoststat = $stmt->fetchrow_array();
    for my $v (@r_mysqlstat) {
        $curr_stat{$kpi[$ii]} = $v;
        $ii ++;
    }
    for my $v (@r_mysqlrt) {
        $curr_stat{$kpi[$ii]} = $v;
        $ii ++;
    }
    for my $v (@r_hoststat) {
        $curr_stat{$kpi[$ii]} = $v;
        $ii ++;
    }
    
    #get baseline data
    my $sql_baseline = <<EOD;
        select sqps , qps , tps , ins , upd , del , sel ,
            connections conn, threads_running act, rt rt_avg,
            load1 , cpu_user cpu_usr, cpu_wait cpu_wai, cpu_sys , net_recv , net_send
        from mysql_baseline
        where instance_name = ?
        and hhmm = ?
EOD
    #log warn
    my $sql_logwarn = <<EOD;
        insert into mysql_warnings(cluster_name, node_name, host_name, instance_name, 
            warn_type, warn_info, check_time)
        values(?,?,?,?,?,?,?)
EOD

    $stmt = $dbh->prepare($sql_baseline);
    my $hhmm = sprintf "%02d%02d", $h, $m;
    $stmt->execute($instance_name, $hhmm);

    my $baseline_stat = $stmt->fetchrow_hashref();

    my $stmt_savewarn = $dbh->prepare($sql_logwarn);

    for my $stat (@kpi) {
        #print "$start_time $stat baseline: $baseline_stat->{$stat}, current_val: $curr_stat{$stat}\n";
        #print "$high_wm, $relative_percent, $low_wm\n";
        my $conf = $warn_thres{$stat};
        my ($high_wm, $relative_percent, $low_wm) = ($conf->[0], $conf->[1], $conf->[2]);
        my $baseline_val = $baseline_stat->{$stat};
        my $current_val = $curr_stat{$stat};
        if($current_val > $high_wm) {
            my $msg = sprintf "$stat more then $high_wm, current is %.02f", $current_val;
            $stmt_savewarn->execute($cluster_name, $node_name, $host_name, $instance_name, "absolute", $msg, $start_time);
            print "$msg\n";
        } else {
            if($baseline_val > $low_wm) {
                my $percent = $current_val / $baseline_val;
                if($percent > $relative_percent) {
                    my $msg = sprintf "$stat increase more then $relative_percent , baseline $baseline_val, current is %.2f, increase, %.2f",$current_val, $percent;
                    $stmt_savewarn->execute($cluster_name, $node_name, $host_name, $instance_name, "baseline", $msg, $start_time);
                    print "$msg\n";
                }
            }
        }
        $dbh->commit();
    }
}


sub gather_warn {
    my ($day, $h, $m) = @_;
    my $dbh = &get_mysql_connection("127.0.0.1:3306", "tianji", "monitor", "monitor");

    my $instance_list = &get_instance_list($dbh);
    
    my $t1 = sprintf "$day %02d:%02d:00", $h, $m;
    my $t2 = sprintf "$day %02d:%02d:59", $h, $m;

    for my $inst(@$instance_list) {
        my ($instance_name, $host_name, $cluster_name, $node_name) = ($inst->[0], $inst->[1], $inst->[2], $inst->[3]);
        #print "###$instance_name, $host_name, $cluster_name, $node_name\n";
        &gather_warn_info_1min($dbh, $cluster_name, $node_name, $instance_name, $host_name, $t1, $t2, $h, $m, $week);
        $dbh->commit();
        $i++;
    }
    #summary
    my $sql_summary = <<EOD;
        replace into mysql_warn_summary (cluster_name, check_time, warn_count)
        select cluster_name, check_time, count(*) 
        from mysql_warnings  
        where check_time >= ?
        and check_time < ?
        group by cluster_name, check_time
EOD
    print "$sql_summary\n$t1, $t2\n";
    my $stmt_summary = $dbh->prepare($sql_summary);
    $stmt_summary->execute($t1, $t2);
    $dbh->commit();
    $dbh->disconnect();
}


sub main {
    my ($last_hour, $last_min) = ( 0, 0);
    while(1) {
        my $target_time = time() - 120;
        my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($target_time);

        my $str_date = sprintf "%04d-%02d-%02d", $year+1900, $mon+1, $mday;

        if($last_hour == $hour and $last_min == $min) {
            printf "$str_date %02d:%02d:%02d sleep 5 seconds\n", $hour, $min, $sec;
            sleep(5);
        } else {
            &gather_warn($str_date, $hour, $min);
            $last_hour = $hour;
            $last_min = $min;
        }
    }
}

&main();

sub get_mysql_connection {
    my ($mysql_instance, $dbname, $user, $pass) = @_;
    my ($ip_addr, $port) = split /:/, $mysql_instance;
    my $db_str = "";
    if(defined($dbname)) {
       $db_str = ":database=$dbname";
    }
    my $str_conn = "dbi:mysql:host=$ip_addr$db_str;port=$port";
    print "[debug] get_mysql_connection $str_conn\n";
    local $SIG{ALRM} = sub { die "Connect to MySQL $ip_addr:$port timed out\n" };
    alarm 20;
    my $mydb = DBI->connect(
        $str_conn,
        "$user", "$pass") or die "Connect to MySQL $ip_addr:$port error:". DBI->errstr;
    $mydb->{FetchHashKeyName} = 'NAME_lc';
    $mydb->{AutoCommit} = 0;
    $mydb->{RaiseError} = 1;
    $mydb->{PrintError} = 1;

    $mydb->do("set names gbk");
    alarm 0;
    return $mydb;
}

sub get_epoch {
    my $time_str = $_[0];
    #print "[debug] get_epoch $time_str\n";
    my ($date, $time) = split / /, $time_str;

    my ($year, $month, $day) = split /-/, $date;
    my ($hour, $minute, $second) = split /:/, $time;
    $time = timelocal($second, $minute, $hour, $day, $month - 1, $year-1900);
    #print "[debug] get_epoch $year, $month, $day, $hour, $minute, $second\n";
    #print "time: $time\n";
    return $time;
}

sub get_day_and_week {
    my $time;
    if(defined($opt{d})) {
        $time = &get_epoch($opt{d});
    } else {
        $time = time() - 3600 * 24;
    }
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($time);
    my $str_date = sprintf "%04d-%02d-%02d", $year+1900, $mon+1, $mday;
    print "$str_date, $wday\n";
    return ($str_date, $wday);
}
