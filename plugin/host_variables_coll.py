#!/usr/bin/python2.6
#coding=utf-8

import subprocess
import socket
import os,sys,datetime,MySQLdb
from numbers import Number
import comm


#hostname
f = open('/proc/sys/kernel/hostname', 'r')
HOST_NAME=f.readline().rstrip()

tianji_db=comm.MySQL()

def process_row(row):
    if not row:
        return None

    vals=list()
    vals.append(HOST_NAME)
    vals.append(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    vals.append(row['name'])
    vals.append(row['value'])

    def packData(value):
        if isinstance(value,str) or isinstance(value,unicode):
            #value=self.escape(value)
            return '"' + value + '"' 
        elif isinstance(value,int):
            return str(value)
        elif isinstance(value,Number):
            return str(value)
        elif isinstance(value,datetime):
            return str(value)
        elif value is None:
            return '"null"'

    s="(" + ",".join(map(packData,vals)) + ")" 
    return s

def check_server():
    proc=subprocess.Popen(["sysctl","-a"],stdout=subprocess.PIPE)
    rows=list()
    while True:
        line=proc.stdout.readline().rstrip()
        if not line:
            break
        name,value=line.split('=')

        name=name.strip()
        value=value.strip()
        
        rows.append({'name':name, 'value':value})

    def addp(value):
        return '`'+value+'`'

    cols=rows[0].keys()
    values=[process_row(row) for row in rows]
    if values:
        sql="INSERT INTO host_variables(host_name,check_time,name,value) values%s ON DUPLICATE KEY UPDATE `check_time`=VALUES(`check_time`),`Value`=VALUES(`Value`)" % (",".join(values) )
        #print sql
        tianji_db.execute(sql)
    

if __name__ == '__main__':
    import random,time,os
    random.seed(os.getpid()+time.time())
    time.sleep( random.random()*6 )
    check_server()
