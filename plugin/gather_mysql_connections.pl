#!/home/oracle/dbaperl/bin/perl
use Time::Local;
#
# Purpose: gather mysql warn info
# rules: threads_running > 20, rt > 100ms, disk, qps > 10000, tps > 10000, slave delay > 100
#
# base line: qps, tps, ins, upd, del, sel, connections, threads_running
#
use DBI;
use Encode;
use Getopt::Long;

###########################################
sub print_usage () {
  my $text = <<EOF;
  
 NAME:
    tbsync

 Example:

 FUNCTION:
    Table structure and table data compare and sync

 PARAMETER:
    most of the parameter is self explained
            only check table structure
        --recheck=n

EOF
 print STDERR $text;
 exit 0;
}

my %opt = (
);

GetOptions(\%opt,
    'd|day=s',           #day 2012-11-11
    'h|help',            # help
    'recheck=i'
) or print_usage();

if ( $opt{h} ) {
    &print_usage();
}

#conn to mysql db using DBI(dbd-mysql)
sub get_mysql_connection {
    my ($mysql_instance, $user, $pass, $db) = @_;
    my ($ip_addr, $port) = split /:/, $mysql_instance;
    my $str_conn = "dbi:mysql:host=$ip_addr;port=$port;db=$db";
    local $SIG{ALRM} = sub { die "Connect to MySQL $ip_addr:$port timed out\n" };
    alarm 20;
    my $mydb = DBI->connect(
        $str_conn,
        "$user", "$pass") or die "Connect to MySQL $ip_addr:$port error:". DBI->errstr;
    $mydb->{FetchHashKeyName} = 'NAME_lc';
    $mydb->{AutoCommit} = 1;
    $mydb->{RaiseError} = 1;
    $mydb->{PrintError} = 1;

    $mydb->do("set names gbk");
    alarm 0;
    return $mydb;
}

sub get_instance_list {
    my ($dbh) = @_;
    my $sql = "select ip, port, cluster, instance_name from db_list where db_type = 'mysql' and check_time > date_sub(now(), interval 12 hour)"; 
    my $stmt = $dbh->prepare($sql);
    $stmt->execute();
    my $rset = $stmt->fetchall_arrayref();
    return $rset;
}

## | cluster       | varchar(30) | YES  | MUL | NULL    |       |
## | instance_name | varchar(30) | NO   | PRI |         |       |
## | user          | varchar(30) | YES  |     | NULL    |       |
## | db            | varchar(30) | YES  |     | NULL    |       |
## | app_ip        | varchar(30) | NO   | PRI |         |       |
## | app_name      | varchar(30) | YES  | MUL | NULL    |       |
## | connections   | smallint(6) | YES  |     | NULL    |       |
## | gmt_create    | datetime    | YES  |     | NULL    |       |
## | check_time    | datetime    | YES  |     | NULL    |       |
sub merge_conn_info {
    my ($old_info, $new_info) = @_;
    my @new_conns = split /,/, $new_info;
    for my $new_conn (@new_conns) {
        $old_info = &merge_conn_info_1($old_info, $new_conn);
    }
    return $old_info;
}


sub merge_conn_info_1 {
    my ($old_info, $new_info) = @_;
    my @conns = split /,/, $old_info;
    my $matched = 0;
    for ( my $i=0; $i < @conns; $i++) {
        my ($user,$db,$num) = split /:/, $conns[$i];
        my ($user2, $db2, $num2) = split /:/, $new_info;
        if($user eq $user2 and $db eq $db2) {
            $num = $num + $num2;
            $matched = 1;
            $conns[$i] = "$user:$db:$num";
            last;
        }
    }
    if($matched) {
        return join ",", @conns;
    } else {
        return "$old_info,$new_info";
    }
}

sub get_app_conns_by_instance {
    my ($dbh, $appinfo_dbh, $tianji_dbh, $cluster, $instance_name) = @_;
    print "get_app_conns: $cluster, $instance_name\n";
    my $sql = "select user,db,host from information_schema.processlist where host <> ' ' ";
    my $stmt = $dbh->prepare($sql);
    $stmt->execute();
    my $conns = $stmt->fetchall_arrayref();
    my %h_conn;
    for my $conn (@$conns) {
        my $user = $conn->[0];
        my $db = $conn->[1];
        my $h = $conn->[2];
        my ($ip, $port) = split /:/, $h;
        if (defined($h_conn{$ip})) {
            $h_conn{$ip}{conn} = $h_conn{$ip}{conn} + 1;
            $h_conn{$ip}{conn_info} = &merge_conn_info($h_conn{$ip}{conn_info}, "$user:$db:1");
        } else {
            $h_conn{$ip}{conn} = 1;
            $h_conn{$ip}{conn_info} = "$user:$db:1";
            my $dns_name =  &get_app_dns($ip);
            $h_conn{$ip}{dns_name} = $dns_name;
            my $app_name =  &get_app_name_from_fqdn($appinfo_dbh, $dns_name);
            if($dns_name eq "UNKNOWN") {
                $app_name = "UNKNOWN";
            }
            $h_conn{$ip}{app_name} = $app_name;
        }
    }
    my %h_app;
    for my $ip (keys %h_conn) {
        my $app_name = $h_conn{$ip}{app_name};
        my $connections = $h_conn{$ip}{conn};
        my $conn_info = $h_conn{$ip}{conn_info};
        if (!defined($h_app{$app_name})) {
            $h_app{$app_name}{total_machine} = 1; 
            $h_app{$app_name}{total_connections} = $connections; 
            $h_app{$app_name}{max_connections} = $connections; 
            $h_app{$app_name}{min_connections} = $connections; 
            $h_app{$app_name}{conn_info} = $conn_info;
        } else {
            $h_app{$app_name}{total_machine} = $h_app{$app_name}{total_machine}  + 1;
            $h_app{$app_name}{total_connections} = $h_app{$app_name}{total_connections} + $connections; 
            $h_app{$app_name}{max_connections} = $connections if ( $h_app{$app_name}{max_connections} < $connections); 
            $h_app{$app_name}{min_connections} = $connections if ( $h_app{$app_name}{min_connections} > $connections); 
            $h_app{$app_name}{conn_info} = &merge_conn_info($h_app{$app_name}{conn_info}, $conn_info);
        }
    }
    for my $app_name (keys %h_app) {
        if(defined($app_name) and $app_name ne "") {
            my $app_info = &get_app_info($appinfo_dbh, $app_name);
            if(defined($app_info)) {
                my $total_machine = $h_app{$app_name}{total_machine} ;
                my $total_connections = $h_app{$app_name}{total_connections};
                my $max_connections = $h_app{$app_name}{max_connections};
                my $min_connections = $h_app{$app_name}{min_connections};
                my $conn_info = $h_app{$app_name}{conn_info};
                print "@$app_info, $total_machine, $total_connections, $max_connections, $min_connections\n";
                &log_app_conn_info($tianji_dbh, $cluster, $instance_name, @$app_info, 
                    $total_machine, $total_connections, $max_connections, $min_connections, $conn_info);
            }
        }
    }
}

sub log_app_conn_info {
    my ($dbh, $cluster, $instance_name, $app_name, $app_name_cn, $owner, $level,
        $total_machine, $total_conn, $max_conn, $min_conn, $conn_info) = @_;

    my $sql1 = <<EOD;
      insert into app_connections_summary(cluster,
        instance_name, app_name, app_name_cn, app_owner, level,
        total_machine, total_conn, max_conn, min_conn, conn_info, check_time )
      values(?,?,?,?,?,?,?,?,?,?,?,now())
      on duplicate key update
        app_name_cn = ?,
        app_owner = ?,
        level = ?,
        total_conn = ?,
        total_machine = ?,
        max_conn = ?,
        min_conn = ?,
        conn_info=?,
        check_time = now()
EOD

    my $sql2 = <<EOD;
      insert into app_connections_stat(cluster,
        instance_name, app_name,
        total_conn, total_machine, max_conn, min_conn, check_time )
      values(?,?,?,?,?,?,?,now())
EOD
    print "$cluster, $instance_name, $app_name, $total_machine, $total_conn, $max_conn, $min_conn\n";

    my $stmt1 = $dbh->prepare($sql1);
    $stmt1->execute($cluster, $instance_name, $app_name, $app_name_cn, $owner, $level,
            $total_machine, $total_conn, $max_conn, $min_conn, $conn_info,
            $app_name_cn, $owner, $level, $total_conn, $total_machine, $max_conn, $min_conn, $conn_info);

    my $stmt2 = $dbh->prepare($sql2);
    $stmt2->execute($cluster, $instance_name, $app_name, $total_conn, $total_machine, $max_conn, $min_conn);

}

sub get_app_dns {
    my ($ip) = @_;
    my $dns = `host $ip`;
    chomp($dns);
    my $dns_name = "UNKNOWN";
    if ($dns =~ /not found/) {
        #unknown
    } elsif ($dns =~ /domain name pointer (.*)\.$/) {
        $dns_name = $1;
    }
    return $dns_name;
}

sub get_app_connections {
    my $tianji_dbh = &get_mysql_connection("mysqltooldb.db.alipay.com:3306", "monitor", "monitor", "tianji");
    my $appinfo_dbh = &get_mysql_connection("viceroymysql-1-1.db.alipay.com:3306", "opsdba", "opsdba", "zpaas");
    my $instances = &get_instance_list($tianji_dbh);
    for my $instance (@$instances) {
        my ($ip, $port, $cluster, $instance_name) = @$instance;
        eval {
            my $dbh = &get_mysql_connection("$ip:$port", "maintain", "u7VHtQC7J6N6", "information_schema");
            &get_app_conns_by_instance($dbh, $appinfo_dbh, $tianji_dbh, $cluster, $instance_name);
        };
    }
}


&get_app_connections();

sub get_app_info {
    my ($dbh, $app_name) = @_;
    my $sql = "select app_name, app_name_cn, owner, level from zappinfo_app where app_name=?";
    my $stmt = $dbh->prepare($sql);
    $stmt->execute($app_name);
    my $app_info = $stmt->fetchrow_arrayref();
    return $app_info;
}

sub get_app_name_from_fqdn {
    my ($dbh, $fqdn) = @_;
    my $sql = "select app_name from zappinfo_server where fqdn = ?";
    my $stmt = $dbh->prepare($sql);
    $stmt->execute($fqdn);
    my ($app_name) = $stmt->fetchrow_array();
    if(!defined($app_name)) {
        ($app_name) = split /[-.]/, $fqdn;
    }
    return $app_name;
}

#sync app info
#
sub get_epoch {
    my $time_str = $_[0];
    #print "[debug] get_epoch $time_str\n";
    my ($date, $time) = split / /, $time_str;

    my ($year, $month, $day) = split /-/, $date;
    my ($hour, $minute, $second) = split /:/, $time;
    $time = timelocal($second, $minute, $hour, $day, $month - 1, $year-1900);
    #print "[debug] get_epoch $year, $month, $day, $hour, $minute, $second\n";
    #print "time: $time\n";
    return $time;
}

sub get_day_and_week {
    my $time;
    if(defined($opt{d})) {
        $time = &get_epoch($opt{d});
    } else {
        $time = time() - 3600 * 24;
    }
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($time);
    my $str_date = sprintf "%04d-%02d-%02d", $year+1900, $mon+1, $mday;
    print "$str_date, $wday\n";
    return ($str_date, $wday);
}
