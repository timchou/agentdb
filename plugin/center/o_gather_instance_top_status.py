#!/usr/bin/python2.6
import comm,datetime

db=comm.MySQL()

LINUX_STAT=['load1','cpu_usr','cpu_sys','cpu_wai','net_recv','net_send']
MYSQL_STAT=['tps','qps','sqps','sel','ins','upd','del','innodb_bp_reads','innodb_bp_read_requests','threads_running','recv','sent']

def run():
    today=datetime.date.today()
    yesterday=today - datetime.timedelta(1)

    start=yesterday.strftime("%Y-%m-%d 00:00:00")
    end=today.strftime("%Y-%m-%d 00:00:00")
    
    PERPAGE=100
    PAGE=0
    while True:
        sql="select distinct(host_name),ip,cluster from db_list LIMIT %d,%d" % (PERPAGE*PAGE, PERPAGE)
        PAGE+=1

        rows=db.the_all(sql)
        if not rows:
            break

        for row in rows:
            host_name=row['host_name']
            ip=row['ip']
            cluster=row['cluster']

            #get linux_stat
            sql="select max(load1) as load1,max(cpu_usr) as cpu_usr,max(cpu_sys) as cpu_sys,max(cpu_wai) as cpu_wai,max(net_recv) as net_recv,max(net_send) as net_send from linux_stat where host_name='%s' and check_time >= '%s' and check_time <= '%s' LIMIT 1" % \
                (host_name, start, end)

            linux_stat=db.the_one(sql)
            
            flag=False
            for key in linux_stat.keys():
                if linux_stat[key]:
                    flag=True
            if not flag:
                continue


            #get mysql_stat
            sql="select instance_name from db_list where host_name='%s'" % host_name
            instances=db.the_all(sql)
            if not instances:
                continue
            for instance in instances:
                instance_name=instance['instance_name']

                sql="select max(tps) as tps,max(qps) as qps,max(sqps) as sqps,max(sel) as sel,max(ins) as ins,max(del) as del,max(upd) as upd,max(innodb_bp_reads) as innodb_bp_reads,max(innodb_bp_read_requests) as innodb_bp_read_requests,max(threads_running) as threads_running,max(recv) as recv,max(sent) as sent from mysql_stat where instance_name='%s' and check_time >= '%s' and check_time <= '%s' LIMIT 1" % \
                    (instance_name, start, end)

                mysql_stat=db.the_one(sql)
                
                flag2=False
                for key in mysql_stat.keys():
                    if mysql_stat[key] is None:
                        flag2=True
                if flag2:
                    continue
                
                data=dict(linux_stat.items() + mysql_stat.items())
                data['instance_name']=instance_name
                data['host_name']=host_name
                data['ip']=ip
                data['cluster']=cluster
                data['check_time']=yesterday.strftime("%Y-%m-%d 00:00:00")
                data['gmt_created']=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

                db.insert('instance_top_status',data,UPDATE=True)
                db.insert('instance_top_status_stat',data,UPDATE=True)





if __name__ == '__main__':
    run()
