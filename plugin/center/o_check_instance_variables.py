#!/usr/bin/python2.6
import comm,datetime

db=comm.MySQL()

def diff():
    page=0
    perpage=100
    while True:
        sql="select * from instance_all_variables LIMIT %d,%d" % (perpage*page,perpage)
        page=page+1
        
        rows=db.the_all(sql)
        if not rows:
            break
        
        for row in rows:
            if row['Variable_name'] in ('timestamp','pseudo_thread_id','open_files_limit'):
                continue

            flag=True
            if row['cnf_Value'].endswith('M') or row['cnf_Value'].endswith('m'):
                try:
                    tmp=int(row['cnf_Value'][:-1])
                    tmp=tmp*1024*1024
                    if str(tmp) == str(row['Value']):
                        flag=False
                except ValueError:
                    pass

            if row['cnf_Value'].endswith('G') or row['cnf_Value'].endswith('g'):
                try:
                    tmp=int(row['cnf_Value'][:-1])
                    tmp=tmp*1024*1024*1024
                    if str(tmp) == str(row['Value']):
                        flag=False
                except ValueError:
                    pass

            #exceptions
            if row['Variable_name'] == 'slave_skip_errors':
                t1=sorted(row['cnf_Value'].split(','))
                t2=sorted(row['Value'].split(','))
                if ','.join(t1) == ','.join(t2):
                    flag=False
            
            if row['Variable_name'] == 'long_query_time' and row['Value'] and row['pre_Value']:
                row['Value']=int(float(row['Value']))
                row['pre_Value']=int(float(row['pre_Value']))

            if str(row['cnf_Value']) in('1','ON') and str(row['Value']) in('1','ON'):
                flag=False

            if str(row['cnf_Value']) in('0','OFF') and str(row['Value']) in('0','OFF'):
                flag=False
            
            if row['cnf_Value'] and isinstance(row['cnf_Value'],basestring):
                row['cnf_Value']=row['cnf_Value'].rstrip('/')
            if row['Value'] and isinstance(row['Value'],basestring):
                row['Value']=row['Value'].rstrip('/')
            if row['pre_Value'] and isinstance(row['pre_Value'],basestring):
                row['pre_Value']=row['pre_Value'].rstrip('/')

            if flag and row['cnf_Value'] and str(row['cnf_Value']) != str(row['Value']):
                data={
                    'host_name'     :row['host_name'],
                    'instance_name' :row['instance_name'],
                    'cluster'       :row['cluster'],
                    'port'          :row['port'],
                    'variable_name' :row['Variable_name'],
                    'cnf_value'     :str(row['cnf_Value']),
                    'current_value' :str(row['Value']),
                    'check_time'    :datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                }
                print data
                db.insert('instance_variables_log',data)
            
            if row['pre_Value'] and row['pre_Value'] != row['Value']:
                data={
                    'host_name'     :row['host_name'],
                    'instance_name' :row['instance_name'],
                    'cluster'       :row['cluster'],
                    'port'          :row['port'],
                    'variable_name' :row['Variable_name'],
                    'current_value' :str(row['Value']),
                    'pre_value'     :str(row['pre_Value']),
                    'check_time'    :datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                }
                print data
                db.insert('instance_variables_log',data)

if __name__ == '__main__':
    diff()
