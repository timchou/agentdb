#!/usr/bin/python2.6
#coding=utf-8

import subprocess,time
import socket
import os,datetime
import fcntl,sys,inspect

import comm

'''
    data_used 
'''

def cal_info():
    db=comm.MySQL()
   
    #curr_date=datetime.datetime.now().strftime("%Y-%m-%d")
    yesterday=datetime.date.today() - datetime.timedelta(1)

    begin_time=yesterday.strftime("%Y-%m-%d 10:00:00")
    end_time=yesterday.strftime("%Y-%m-%d 11:00:00")
    
    sql="select * from db_list"
    rows=db.the_all(sql)
    if not rows:
        exit() 

    for row in rows:
        data={
            'instance_name' :row['instance_name'],
            'host_name'     :row['host_name'],
            'cluster_name'  :row['cluster'],
            'ip'            :row['ip'] ,
            'port'          :row['port'],
            'data_used'     :row['datadir_used'],
            'check_time'    :yesterday.strftime("%Y-%m-%d"),
        }
        
        sql="select qps,tps,threads_connected,sqps,sel,ins,upd,del from mysql_stat where instance_name='%s' and check_time >= '%s' and check_time <= '%s'" % (row['instance_name'] , begin_time, end_time)
        stats=db.the_all(sql)
        if not stats:
            print "no states,50,%s" % sql
            continue

        max_qps=0;
        max_tps=0;
        max_conn=0;
        max_sqps=0;
        max_sel=0;
        max_ins=0;
        max_upd=0;
        max_del=0;
        if stats:
            tmp=zip(*[iter(stats)]*10)
            if tmp:
                stats=tmp
            else:
                stats=[stats,]
            list_qps=list()
            list_tps=list()
            list_conn=list()
            list_sqps=list()
            list_sel=list()
            list_ins=list()
            list_upd=list()
            list_del=list()
            for stat in stats:
                sum_qps=0
                sum_tps=0
                sum_conn=0
                sum_sqps=0
                sum_sel=0
                sum_ins=0
                sum_upd=0
                sum_del=0
                for s in stat:
                    sum_qps=sum_qps + s['qps']
                    sum_tps=sum_tps + s['tps']
                    sum_conn=sum_conn + s['threads_connected']
                    sum_sqps=sum_qps + s['sqps']
                    sum_sel=sum_sel + s['sel']
                    sum_ins=sum_ins + s['ins']
                    sum_upd=sum_upd + s['upd']
                    sum_del=sum_del + s['del']

                length=len(stat)
                if length > 0:
                    list_qps.append(sum_qps / length)
                    list_tps.append(sum_tps / length)
                    list_conn.append(sum_conn / length)
                    list_sqps.append(sum_sqps / length)
                    list_sel.append(sum_sel / length)
                    list_ins.append(sum_ins / length)
                    list_del.append(sum_del / length)
                    list_upd.append(sum_upd / length)

            max_qps=max(list_qps)
            max_tps=max(list_tps)
            max_conn=max(list_conn)
            max_sqps=max(list_sqps)
            max_sel=max(list_sel)
            max_ins=max(list_ins)
            max_upd=max(list_upd)
            max_del=max(list_del)

        data['peek_qps']=max_qps
        data['peek_tps']=max_tps
        data['peek_connections']=max_conn
        data['peek_sqps']=max_sqps
        data['peek_sel']=max_sel
        data['peek_ins']=max_ins
        data['peek_upd']=max_upd
        data['peek_del']=max_del

        #linux stat
        sql="select load1,net_recv,net_send from linux_stat where host_name='%s' and check_time >= '%s' and check_time <= '%s'" % (row['host_name'] , begin_time, end_time)
        stats=db.the_all(sql)
        max_load1=0
        max_net_recv=0
        max_net_send=0

        if not stats:
            print "no states,80,%s" % sql
            continue
        if stats:
            stats=zip(*[iter(stats)]*10)
            list_load1=list()
            list_net_recv=list()
            list_net_send=list()
            for stat in stats:
                sum_load1=0
                sum_net_recv=0
                sum_net_send=0
                for s in stat:
                    sum_load1=sum_load1+ s['load1']
                    sum_net_recv=sum_net_recv+ s['net_recv']
                    sum_net_send=sum_net_send+ s['net_send']

                length=len(stat)
                if length > 0:
                    list_load1.append(sum_load1 / length)
                    list_net_recv.append(sum_net_recv / length)
                    list_net_send.append(sum_net_send / length)

            max_load1=max(list_load1)
            max_net_recv=max(list_net_recv)
            max_net_send=max(list_net_send)

        data['load1']=max_load1
        data['net_recv']=max_net_recv
        data['net_send']=max_net_send 
        
        #rt
        sql="select rt_avg from rt_tcprstat where instance_name='%s' and check_time >= '%s' and check_time <= '%s'" % (row['instance_name'] , begin_time, end_time)
        stats=db.the_all(sql)
        max_rt=0

        if not stats:
            print "no states,80,%s" % sql
            continue
        if stats:
            tmp=zip(*[iter(stats)]*10)
            if tmp:
                stats=tmp
            else:
                stats=[stats,]
            list_rt=list()
            for stat in stats:
                sum_rt=0
                for s in stat:
                    sum_rt=sum_rt+ s['rt_avg']

                length=len(stat)
                if length > 0:
                    list_rt.append(sum_rt / length)
            max_rt=max(list_rt)
            
        data['peek_rt']=max_rt

        #total query,
        t1=yesterday.strftime("%Y-%m-%d 00:00:00")
        t2=datetime.datetime.now().strftime("%Y-%m-%d 00:00:00")
        sql="select sum(qps) as total_query,sum(tps) as total_dml from mysql_stat where instance_name='%s' and check_time >= '%s' and check_time <= '%s'" % (row['instance_name'] , t1 , t2) 
        line=db.the_one(sql)
        if line:
            if line['total_query']:
                data['total_query']=int(line['total_query'])
            else:
                data['total_query']=0

            if line['total_dml']:
                data['total_dml']=int(line['total_dml'])
            else:
                data['total_dml']=0
        print data
        #exit()
        db.insert('mysql_stat_daily_sum',data,UPDATE=True)


if __name__ == '__main__':
    #pid=str(os.getpid())
    #pidfile='/tmp/' + os.path.basename(sys.argv[0]) + '.lock'
    #if os.path.isfile(pidfile):
    #    print "%s already exists, exiting" % pidfile
    #    sys.exit()
    #else:
    #    file(pidfile, 'w').write(pid)

    cal_info()

    #os.unlink(pidfile)
