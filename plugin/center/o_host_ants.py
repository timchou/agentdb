#!/usr/bin/python2.6
#coding=utf-8
from __future__ import division
import subprocess,time
import socket
import os,datetime
import fcntl,sys,inspect

import comm

def addp(str):
    return '"' + str + '"'


def cal_info():
    db=comm.MySQL()
    now=datetime.datetime.now()
    yesterday=datetime.date.today() - datetime.timedelta(1)

    t1=yesterday.strftime("%Y-%m-%d 00:00:00")
    t2=now.strftime("%Y-%m-%d 00:00:00")
    curr=now.strftime("%Y-%m-%d %H:%M:%S")

    sql="select * from host_list"
    rows=db.the_all(sql)
    if not rows:
        return

    ips=list()
    for row in rows:
        ips.append(row['ip'])

    for ip in list(ips):
        if not ip:
            ips.remove(ip)
    if not ips:
        return
    sql="select count(*) as cnt ,ip from db_list where ip in (%s) group by ip" % ",".join(map(addp,ips))
    instances=db.the_all(sql)
    ips=dict()
    if instances:
        for i in instances:
            ips[i['ip']]=int(i['cnt'])

    for row in rows:
        sql="select * from disk_space_stat where host_name='%s' and mount in ('/log','/data','/u01') and check_time >= '%s' and check_time <= '%s'" % \
            (row['host_name'],t1,t2)
        res=db.the_all(sql)
        if not res:
            continue

        score=0
        for r in res:
            if r['mount'] == '/data':
                score=int(r['capacity']) / 100

        if row['ip'] not in ips:
            ips[row['ip']]=0

        data={
            'host_name'  :row['host_name'],
            'check_time' :curr,
            'ip'         :row['ip'],
            'instances'  :ips[row['ip']],
            'score'      :score
        }
        db.insert('ants',data,UPDATE=True)
        data['stat_time']=yesterday.strftime("%Y-%m-%d")
        db.insert('ants_stat',data,UPDATE=True)

if __name__ == '__main__':
    cal_info()
