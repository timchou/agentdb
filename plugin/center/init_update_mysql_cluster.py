#!/usr/bin/python2.6
#coding=utf-8

import subprocess,time
import socket
import os,datetime,time
import fcntl,sys,inspect

import comm

'''
mysql_cluster info coll
'''


def cal_info():
    db=comm.MySQL()
    
    #now=datetime.datetime.now()
    #yesterday=datetime.date.today() - datetime.timedelta(1)
    now=datetime.date(2012,7,20)
    yesterday=now - datetime.timedelta(1)

    sql="select distinct(cluster) from db_list"
    rows=db.the_all(sql)
    if not rows:
        return

    while True:
        now=now + datetime.timedelta(1)
        yesterday=yesterday+ datetime.timedelta(1)
        if now == datetime.date.today():
            break
    
        begin_time=yesterday.strftime("%Y-%m-%d 10:00:00")
        end_time=yesterday.strftime("%Y-%m-%d 11:00:00")
    
        for row in rows:
            cluster=row['cluster']
            if not cluster:
                continue

            data={
                'cluster_name'  :cluster,
                'check_time'    :now.strftime("%Y-%m-%d %H:%M:%S"),
                'data_total'    :0,
                'data_used'     :0,
                }

            sql="select * from db_list where cluster='%s'" % cluster
            instances=db.the_all(sql)
            if not instances:
                continue

            sql="select host_name,sum(peek_qps) as peek_qps,sum(peek_tps) as peek_tps,max(peek_rt) as peek_rt,sum(total_query) as total_query,sum(total_dml) as total_dml,sum(peek_connections) as peek_connections ,sum(peek_sel) as peek_sel,sum(peek_ins) as peek_ins,sum(peek_upd) as peek_upd,sum(peek_del) as peek_del,max(load1) as peek_load1,sum(net_recv) as peek_net_recv,sum(peek_sqps) as peek_sqps,sum(net_send) as peek_net_send from mysql_stat_daily_sum where cluster_name='%s' and check_time='%s'" % ( cluster , yesterday.strftime("%Y-%m-%d"))
            instances_stat=db.the_one(sql)
            if not instances_stat or not instances_stat['host_name'] or instances_stat['peek_sel'] is None:
                print 'no data'
                continue
            
            data['machine_count']=len(instances)
            for i in instances:
                data['data_total'] = data['data_total'] + int(i['datadir_total'])
                data['data_used'] = data['data_used'] + int(i['datadir_used'])

            data['peek_qps']=int(instances_stat['peek_qps'])
            data['peek_tps']=int(instances_stat['peek_tps'])
            data['peek_rt']=int(instances_stat['peek_rt'])
            data['total_query']=int(instances_stat['total_query'])
            data['total_dml']=int(instances_stat['total_dml'])
            data['connections']=int(instances_stat['peek_connections'])

            data['peek_sel']=int(instances_stat['peek_sel'])
            data['net_send']=int(instances_stat['peek_net_send'])
            data['peek_upd']=int(instances_stat['peek_upd'])
            data['peek_sqps']=int(instances_stat['peek_sqps'])
            data['peek_ins']=int(instances_stat['peek_ins'])
            data['load1']=int(instances_stat['peek_load1'])
            data['peek_del']=int(instances_stat['peek_del'])
            data['net_recv']=int(instances_stat['peek_net_recv'])

            data['qps_gather_time_begin']=begin_time
            data['qps_gather_time_end']=end_time
            data['tps_gather_time_begin']=begin_time
            data['tps_gather_time_end']=end_time
            
            #db.insert('mysql_cluster',data,UPDATE=True) 
            #print data

            data_stat=dict()
            data_stat['cluster_name']=data['cluster_name']
            data_stat['machine_count']=data['machine_count']
            data_stat['data_total']=data['data_total']
            data_stat['data_used']=data['data_used']
            data_stat['peek_qps']=data['peek_qps']
            data_stat['peek_tps']=data['peek_tps']
            data_stat['total_query']=data['total_query']
            data_stat['total_dml']=data['total_dml']
            data_stat['connections']=data['connections']
            data_stat['check_time']=data['check_time']
            data_stat['peek_sel']=data['peek_sel']
            data_stat['net_send']=data['net_send']
            data_stat['peek_upd']=data['peek_upd']
            data_stat['peek_sqps']=data['peek_sqps']
            data_stat['peek_ins']=data['peek_ins']
            data_stat['load1']=data['load1']
            data_stat['peek_del']=data['peek_del']
            data_stat['net_recv']=data['net_recv']
            
            print data_stat
            #exit()
            db.insert('mysql_cluster_stat',data_stat,UPDATE=True) 
            #time.sleep(0.3)

if __name__ == '__main__':
    #pid=str(os.getpid())
    #pidfile='/tmp/' + os.path.basename(sys.argv[0]) + '.lock'
    #if os.path.isfile(pidfile):
    #    print "%s already exists, exiting" % pidfile
    #    sys.exit()
    #else:
    #    file(pidfile, 'w').write(pid)

    cal_info()

    #os.unlink(pidfile)
