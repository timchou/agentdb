#!/usr/bin/python2.6

from comm import MySQL
import _mysql_exceptions


def find_his_master(conn):
    if not conn:
        return False
    sql="select @@read_only as ro"
    row=conn.the_one(sql)

    host=None
    port=None
    #read_only,find his master
    if row['ro'] == 1:
        sql="show slave status"
        row=conn.the_one(sql)
        if not row:
            print 'read_only is 1,and show slave status returns nothing',vars(conn)
            return None
        try:
            db2 = MySQL(host=row['Master_Host'], user='maintain', password='u7VHtQC7J6N6', database='information_schema',port=row['Master_Port'])
            res=find_his_master(db2)
            if res:
                host=res['host']
                port=res['port']
        except _mysql_exceptions.OperationalError:
            print 'cannot connect %s:%d' % (row['Master_Host'],row['Master_Port'])
    else:
        host=conn.host
        port=conn.port

    return {'host':host,'port':port}

def process_dead(row):
    print "find dead !!",row

    if row['backup_ips']:
        bps=row['backup_ips'].split(',')
        for bp in bps:
            host,port=bp.split(':')
            try:
                db2 = MySQL(host=host, user='maintain', password='u7VHtQC7J6N6', database='information_schema',port=int(port) )
                res=find_his_master(db2)

                sql="update node_list set ip='%s',port=%d where id=%d" % (res['host'],res['port'],row['id'])
                db.execute(sql)
                print 'UPDATED_DEAD!! %s:%d == %s:%d' % (row['ip'],row['port'],res['host'],res['port'])

            except _mysql_exceptions.OperationalError:
                continue



def maintain():
    sql="select * from node_list"
    db=MySQL()
    rows=db.the_all(sql)
    if not rows:
        return 1

    for row in rows:
        try:
            db2 = MySQL(host=row['ip'], user='maintain', password='u7VHtQC7J6N6', database='information_schema',port=row['port'])
            res=find_his_master(db2)

            if res and res['host'] and res['port']:
                if row['ip'] != res['host'] or row['port'] != res['port']:
                    sql="update node_list set ip='%s',port=%d where id=%d" % (res['host'],res['port'],row['id'])
                    print sql
                    db.execute(sql)
                    print 'UPDATED!! %s:%d == %s:%d' % (row['ip'],row['port'],res['host'],res['port'])

        except _mysql_exceptions.OperationalError:
            exit()
            process_dead(row)


if __name__ == '__main__':
    import random,time,os
    random.seed(os.getpid())
    time.sleep( random.random()*60 )
    maintain()
