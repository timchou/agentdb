#!/usr/bin/python2.6

from comm import MySQL
import _mysql_exceptions

USER='maintain'
PWD='u7VHtQC7J6N6'

PORTS=[3306,3316,3317,3307,3308,3309,3310,3311,3312,3313,3314,3315,3318,3319,3320,3321]

db=MySQL()

def update_one_ins_list(data,processed_instances):
    '''
    db2 is the connection to the mysql
    row contain 'node' and 'cluser'
    .then fine all db2's slave info ,then update HOST_LIST
    '''
    ip_name="%s:%d" % (data['ip'] , data['port'])
    if ip_name in processed_instances:
        return

    #print 'check info for:',data['ip'],data['port'],data['cluster'],data['node']

    #get it's instance info from tianji
    sql="select * from db_list where ip='%s' and port=%d limit 1" % (data['ip'] , data['port'])
    instance_info=db.the_one(sql)
    if not instance_info:
        return

    instance_name="%s:%d" % (instance_info['host_name'] , instance_info['port'])

    try:
        db2 = MySQL(host=data['ip'], user=USER, password=PWD, database='information_schema',port=data['port'])
    except _mysql_exceptions.OperationalError:
        print 'cant conn'
        return False

    #find param info
    sql="select @@server_id as serverid,@@read_only as readonly,count(*) as conn from information_schema.processlist"
    param_info=db2.the_one(sql)
    if not param_info:
        print 'no param_info'
        return False

    #find slaves
    sql="select HOST from information_schema.processlist where COMMAND='Binlog Dump'"
    rows=db2.the_all(sql)

    slaveinfo=''
    slave_infos=list()
    if rows:
        #master
        temp_list=list()
        for row in rows:
            ip,_=row['HOST'].split(':')

            for port in PORTS:
                try:
                    db3 = MySQL(host=ip, user=USER, password=PWD, database='information_schema',port=port)
                    sql='show slave status'
                    res=db3.the_one(sql)
                    if res and res['Master_Host'] == data['ip'] and res['Master_Port'] == data['port']:

                        i="%s:%d" % (ip,port)
                        if i not in temp_list:
                            temp={'ip':ip,'port':port}
                            slave_infos.append(temp)
                            temp_list.append(i)
                            break
                except _mysql_exceptions.OperationalError:
                    continue

        if slave_infos:
            for slave in slave_infos:
                slaveinfo += slave['ip'] + ':' + str(slave['port']) + ';'


    sql="update db_list set cluster='%s',node='%s',is_readonly=%d,slaveinfo='%s',conn=%d,serverid=%d where instance_name='%s'" % \
            (data['cluster'] , data['node'], param_info['readonly'],slaveinfo,param_info['conn'],param_info['serverid'],instance_name)

    print sql
    db.execute(sql)
    processed_instances.append(ip_name)

    for slave in slave_infos:
        temp={
            'cluster'   :data['cluster'],
            'node'      :data['node'],
            'ip'        :slave['ip'],
            'port'      :slave['port'],
            }
        update_one_ins_list(temp,processed_instances)



def maintain():
    sql="select * from node_list"
    rows=db.the_all(sql)
    if not rows:
        return 1

    for row in rows:
        processed_instances=list()
        update_one_ins_list(row,processed_instances)



if __name__ == '__main__':
    import random,time,os
    random.seed(os.getpid())
    time.sleep( random.random()*60 )
    maintain()
