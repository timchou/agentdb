#!/usr/bin/python2.6
from __future__ import division
import argparse,json,urllib2,datetime,socket,json,subprocess,time,os
from comm import MySQL
import MySQLdb

parser = argparse.ArgumentParser(description='console for orzdba')
parser.add_argument("-p","--port", help="port...")
args = parser.parse_args()

#find mysql
MYSQL_BINS=['/home/oracle/mysql/bin/mysql','/usr/bin/mysql']
MYSQL_BIN=None
for m in MYSQL_BINS:
    if os.path.exists(m):
        MYSQL_BIN=m

MYSQL=MYSQL_BIN + " -s --skip-column-names -udbadmin -palipswxx -P%d "

def execute(cmd):
    proc=subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE)
    return proc.stdout.readlines()

def processlist(port):
    if not port:
        return

    infos=dict()

    #date\host\ip
    infos['date']=str(datetime.date.today())
    infos['datetime']=datetime.datetime.now().strftime("%H:%M:%S")
    f = open('/proc/sys/kernel/hostname', 'r')
    infos['host_name']=f.readline().rstrip()
    infos['ip']=socket.gethostbyname(infos['host_name'])

    #schema\vars
    #cmd = MYSQL + '"show databases" | grep -iwvE "information_schema|mysql|test" | tr "\\n" "|"'
    #infos['schemas']=execute(cmd)[0]
    infos['port']=port

    #cmd = MYSQL + "'%s'" % 'show variables where Variable_name in ("read_only","version","sync_binlog","max_connections","max_user_connections","max_connect_errors","table_open_cache","table_definition_cache","thread_cache_size","binlog_format","open_files_limit","max_binlog_size","max_binlog_cache_size","innodb_flush_log_at_trx_commit","innodb_flush_method","innodb_buffer_pool_size","innodb_max_dirty_pages_pct","innodb_log_buffer_size","innodb_log_file_size","innodb_log_files_in_group","innodb_thread_concurrency","innodb_file_per_table","innodb_adaptive_hash_index","innodb_open_files","innodb_io_capacity","innodb_read_io_threads","innodb_write_io_threads","innodb_adaptive_flushing","innodb_lock_wait_timeout","innodb_log_files_in_group","innodb_stats_on_metadata","innodb_buffer_pool_instances")'
    #lines=execute(cmd)
    #for line in lines:
    #    line=line.strip()
    #    key,val=line.split("\t")
    #    infos[key]=val

    f = open('/proc/loadavg', 'r')
    lines=f.readline().rstrip().split()
    infos['load1']=lines[0]
    infos['load5']=lines[1]
    infos['load15']=lines[2]


    f = open('/proc/stat', 'r')
    t=f.readline().rstrip()[3:].strip().split()
    infos['cpu_user']=t[0]
    infos['cpu_nic']=t[1]
    infos['cpu_sys']=t[2]
    infos['cpu_idle']=t[3]
    infos['cpu_iowait']=t[4]
    infos['cpu_irq']=t[5]
    infos['cpu_softirq']=t[6]

    cmd="cat /proc/vmstat | grep -E 'pswpin|pswpout'"
    lines=execute(cmd)
    for line in lines:
        line=line.strip()
        key,val=line.split()
        infos[key]=val

    cmd = MYSQL + "'%s'" % 'show global status where Variable_name in ("Com_select","Com_insert","Com_update","Com_delete","Innodb_buffer_pool_read_requests","Innodb_buffer_pool_reads","Threads_running","Threads_connected","Threads_cached","Threads_created")'
    lines=execute(cmd)
    for line in lines:
        line=line.strip()
        key,val=line.split("\t")
        infos[key]=val

    infos['hit']=str(round( (int(infos['Innodb_buffer_pool_read_requests']) - int(infos['Innodb_buffer_pool_reads'])) / int(infos['Innodb_buffer_pool_read_requests']) * 100,2))
    #infos['innodb_buffer_pool_size']=str(round( int(infos['innodb_buffer_pool_size']) / 1024 / 1024 / 1024 , 2)) + 'G'
    #infos['max_binlog_size']=str(round( int(infos['max_binlog_size']) / 1024 / 1024 / 1024 , 2)) + 'G'
    #infos['innodb_log_buffer_size']=str(round( int(infos['innodb_log_buffer_size']) / 1024 / 1024 / 1024 , 2)) + 'G'
    #infos['innodb_log_file_size']=str(round( int(infos['innodb_log_file_size']) / 1024 / 1024 / 1024 , 2)) + 'G'
    #infos['max_binlog_cache_size']=str(round( int(infos['max_binlog_cache_size']) / 1024 / 1024 / 1024 , 2)) + 'G'

    infos['lor']=infos['Innodb_buffer_pool_read_requests']

    infos['processlist']=list()
    infos['COMMAND']=dict()
    infos['STATE']=dict()
    #infos['Sleep']=0
    #infos['Binlog_Dump']=0
    #infos['Query']=0
    #infos['Connect']=0
    #infos['Total']=0

    conn = MySQLdb.connect(host='127.0.0.1', user='dbadmin', passwd='alipswxx', db='information_schema',port=port,charset="gbk")
    conn.autocommit(1)
    cursor = conn.cursor(MySQLdb.cursors.DictCursor)
    sql="select * from information_schema.PROCESSLIST"
    cursor.execute(sql.encode('gbk'))
    rows=cursor.fetchall()
    if rows:
        infos['Total']=len(rows)
        for row in rows:
            #if row['COMMAND'] == 'Sleep':
            #    infos['Sleep']+=1
            #    continue

            #if row['COMMAND'] == 'Connect':
            #    infos['Connect']+=1
            #    continue

            #if row['COMMAND'] == 'Binlog Dump':
            #    infos['Binlog_Dump']+=1
            #    continue

            #if row['COMMAND'] == 'Query':
            #    infos['Query']+=1
            if row['COMMAND']:
                if row['COMMAND'] not in infos['COMMAND']:
                    infos['COMMAND'][row['COMMAND']]=0
                infos['COMMAND'][row['COMMAND']] += 1

            if row['STATE']:
                if row['STATE'] not in infos['STATE']:
                    infos['STATE'][row['STATE']]=0
                infos['STATE'][row['STATE']] += 1

            if row['COMMAND'] == 'Query':
                p={
              'ID':row['ID'],
              'HOST':row['HOST'],
              'USER':row['USER'],
              'DB':row['DB'],
              'COMMAND':row['COMMAND'],
              'TIME':row['TIME'],
              'STATE':row['STATE'],
              'INFO':row['INFO'],
                }
                infos['processlist'].append(p)

    cmd="grep  'bond0:' /proc/net/dev"
    lines=execute(cmd)
    lines=lines[0]
    lines=lines.split()
    infos['net_rec']=lines[1]
    infos['net_sent']=lines[8]

    #for i in infos.keys():
    #    print i,infos[i]

    print json.dumps(infos)


if __name__ == '__main__':
    if args.port:
        os.environ['MYSQL_HOME'] = "/data/mysql%s" % args.port


        a="ps -elf | grep mysqld| grep -v mysqld_safe|grep -v grep | grep " + args.port + "| awk '{ s = \"\"; for (i = 15; i <= NF; i++) s = s $i \" \"; print s }'"
        proc=subprocess.Popen(a,shell=True,stdout=subprocess.PIPE)
        S=""
        while True:
            line=proc.stdout.readline().rstrip()
            if not line:
                break
            nfields=len(line.split()) - 1
            inst=line.split(None,nfields)

            if inst:
                for fi in inst:
                    if fi.startswith('--socket'):
                        _,S=fi.split('=')
                        S=" --socket="+S


        MYSQL=MYSQL % int(args.port)
        MYSQL=MYSQL + S + " -e "

        processlist(int(args.port))
