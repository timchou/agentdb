. /home/oracle/.bash_profile

mysql -umonitor -pmonitor -h mysqltooldb.db.alipay.com tianji -vvv <<EOD

insert ignore into disk_space_by_instance(instance_name, host_name, port, 
    datadir_total, datadir_used, datadir_free, 
    check_date, cluster)
select instance_name, host_name, port,
    datadir_total, datadir_used, 0,
    date_format(check_time, '%Y-%m-%d'), cluster
from db_list
where db_type = 'mysql';

update disk_space_by_instance a, disk_space_by_instance b
set a.disk_increment = a.datadir_used - b.datadir_used
where a.check_date = date_add(b.check_date, interval 1 day)
and a.instance_name = b.instance_name
and a.check_date = current_date();

EOD
