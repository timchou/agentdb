#!/usr/bin/python2.6
#coding=utf-8

import subprocess
import socket
import os,sys,inspect,datetime,re,time
import comm


infos=dict()
            


db=comm.MySQL()

def get_host_log():
    pos_file='/tmp/.' + os.path.basename(sys.argv[0]) + '.host.pos'
    f_pos=0
    pos=0
    if os.path.isfile(pos_file):
        f_pos=open(pos_file,'r+')
        pos=f_pos.readline()
        if pos:
            pos=int(pos.strip())
    else:
        f_pos=open(pos_file,'w+')

    log_file="/var/log/messages"
    if not os.path.isfile(log_file):
        return None

    f_log=open(log_file,'r')
    f_log.seek(pos,0)

    while True:
        line=f_log.readline()
        if not line:
            break
        print '111 ' + line
    
    curr_pos=f_log.tell()

    f_pos.seek(0,0)
    f_pos.write(str(curr_pos))

def get_mysql_log():
    rows=list()

    args="ps -elf | grep mysqld| grep -v mysqld_safe|grep -v grep | awk '{ s = \"\"; for (i = 15; i <= NF; i++) s = s $i \" \"; print s }'"
    proc=subprocess.Popen(args,shell=True,stdout=subprocess.PIPE)
    instances=list()
    while True:
        line=proc.stdout.readline().rstrip()
        if not line:
            break
        nfields=len(line.split()) - 1 
        inst=line.split(None,nfields)

        instance=dict()
        if inst:
            for fi in inst:
                if fi.startswith('--port'):
                    _,instance['port']=fi.split('=')
                if fi.startswith('--log-error'):
                    _,instance['log_file']=fi.split('=')
        instances.append(instance)

    
    for instance in instances:
        if not instance:
            continue

        port=int(instance['port'])
        log_file=instance['log_file']

        pos_file='/tmp/.' + os.path.basename(sys.argv[0]) + '.mysql.'+str(port)+'.pos'
        f_pos=0
        pos=0
        log_file_fromwhat=0
        if os.path.isfile(pos_file):
            f_pos=open(pos_file,'r+')
            pos=f_pos.readline()
            if pos:
                pos=int(pos.strip())
        else:
            f_pos=open(pos_file,'w+')
            log_file_fromwhat=0
        
        if not pos:
            pos=0

        if not os.path.isfile(log_file):
            return None

        f_log=open(log_file,'r')
        f_log.seek(pos,log_file_fromwhat)
        
        row=dict()
        while True:
            line=f_log.readline()
            if not line:
                break

            if re.search("((?=.*Note)(?=.*mysqld)(?=.*(ready for connections)).*)" , line):
                tmp=line.split()
                if tmp:
                    t=time.strptime(tmp[0] + ' ' + tmp[1] , "%y%m%d %H:%M:%S")
                    row['mysql_port']=port
                    row['level']=2
                    row['action']="mysqld_start"
                    row['action_time']=time.strftime("%Y-%m-%d %H:%M:%S" ,t)
                    rows.append(row)
        
        curr_pos=f_log.tell()

        f_pos.seek(0,0)
        f_pos.write(str(curr_pos))

    return rows

def check_server():
    #hostname
    f = open('/proc/sys/kernel/hostname', 'r')
    HOST_NAME=f.readline().rstrip()

    #SN
    proc=subprocess.Popen("/usr/sbin/dmidecode -t 1 | grep -i seri",shell=True,stdout=subprocess.PIPE)
    sn=proc.stdout.readline().strip()
    sn=sn.split(':')
    SN=sn[1]
    
    #host_log=get_host_log()
    mysql_rows=get_mysql_log()
    for row in mysql_rows:
        row['host_name']=HOST_NAME
        row['ip']=socket.gethostbyname(HOST_NAME)
        row['sn']=SN
        db.insert('comm_logs',row)

if __name__ == '__main__':
    import random,time,os
    random.seed(os.getpid()+time.time())
    time.sleep( random.random()*6 )
    check_server()
