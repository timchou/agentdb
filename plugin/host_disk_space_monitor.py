#!/usr/bin/python2.6
#coding=utf-8

import subprocess,time
import socket
import os,datetime
import fcntl,sys,inspect

import comm

#disk util threshold
WARNING_PER=40
HOSTNAME=socket.gethostname()


def insert_into_db(infos):
    if not infos:
        return False

    db=comm.MySQL()
    db.insert('disk_space',infos,UPDATE=True)
    db.insert('disk_space_stat',infos)


def check_disk():
    proc=subprocess.Popen("df -lmP | awk '{print $1,$2,$3,$4,$5,$6}'",shell=True,stdout=subprocess.PIPE)
    
    infos=list()
    skip_first=True
    while True:
        line=proc.stdout.readline()
        if skip_first:
            skip_first=False
            continue
        if line != '':
            a=line.rstrip().split()
            if a:
                infos.append(a)
        else:
            break
    
    curr_time=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    for i in infos:
        per=int( i[4].rstrip('% ') )
        data={
            'host_name' :   HOSTNAME,
            'mount'     :   i[5],
            'device'    :   i[0],
            'size'      :   int(i[1]),
            'used'      :   int(i[2]),
            'available' :   int(i[3]),
            'capacity'  :   per ,
            'check_time':   curr_time 
        }
	#print data
        insert_into_db(data)

if __name__ == '__main__':
    import random,time,os
    random.seed(os.getpid()+time.time())
    time.sleep( random.random()*6 )
    check_disk()

