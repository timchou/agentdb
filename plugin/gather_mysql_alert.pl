#!/home/oracle/dbaperl/bin/perl
use Time::Local;
#
# Purpose: gather mysql warn info
# rules: threads_running > 20, rt > 100ms, disk, qps > 10000, tps > 10000, slave delay > 100
#
# base line: qps, tps, ins, upd, del, sel, connections, threads_running
#
use DBI;
use Encode;
use Getopt::Long;

###########################################
sub print_usage () {
  my $text = <<EOF;
  
 NAME:
    tbsync

 Example:

 FUNCTION:
    Table structure and table data compare and sync

 PARAMETER:
    most of the parameter is self explained
            only check table structure
        --recheck=n

EOF
 print STDERR $text;
 exit 0;
}

my %opt = (
);

GetOptions(\%opt,
    'd|day=s',           #day 2012-11-11
    'h|help',            # help
    'recheck=i'
) or print_usage();

if ( $opt{h} ) {
    &print_usage();
}

#conn to mysql db using DBI(dbd-mysql)
sub get_mysql_connection {
    my ($mysql_instance, $user, $pass, $db) = @_;
    my ($ip_addr, $port) = split /:/, $mysql_instance;
    my $str_conn = "dbi:mysql:host=$ip_addr;port=$port;db=$db";
    local $SIG{ALRM} = sub { die "Connect to MySQL $ip_addr:$port timed out\n" };
    alarm 20;
    my $mydb = DBI->connect(
        $str_conn,
        "$user", "$pass") or die "Connect to MySQL $ip_addr:$port error:". DBI->errstr;
    $mydb->{FetchHashKeyName} = 'NAME_lc';
    $mydb->{AutoCommit} = 1;
    $mydb->{RaiseError} = 1;
    $mydb->{PrintError} = 1;

    $mydb->do("set names gbk");
    alarm 0;
    return $mydb;
}


sub sync_mysql_alert {
    my ($sync_time) = @_;
    my $monitor_dbh = &get_mysql_connection("nagiosdb.db.alipay.com:3306", "opsdba", "opsdba", "subscribe");
    my $tianji_dbh = &get_mysql_connection("mysqltooldb.db.alipay.com:3306", "monitor", "monitor", "tianji");
    my $last_min = -1;
    my $mysql_meta = &get_mysql_meta($tianji_dbh);

    if(defined($sync_time)) {
        &sync_mysql_alert_by_person_inc($monitor_dbh, $tianji_dbh, $sync_time, $mysql_meta);
        &sync_mysql_alert_inc($monitor_dbh, $tianji_dbh, $sync_time, $mysql_meta);
    } 

    while(1) {
        my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
        if($last_min == $min) {
            sleep(5);
        } else {
            my ($last_sync_time1, $last_sync_time2) = &get_last_sync_time($tianji_dbh);
            if(!defined($last_sync_time1) or !defined($last_sync_time2)) {
                print "exit here\n";
                return 0;
            }
            &sync_mysql_alert_inc($monitor_dbh, $tianji_dbh, $last_sync_time1, $mysql_meta);
            &sync_mysql_alert_by_person_inc($monitor_dbh, $tianji_dbh, $last_sync_time2, $mysql_meta);
            $last_min = $min;
        }
        if($min % 5 == 0) {
            $mysql_meta = &get_mysql_meta($tianji_dbh);
        }
    }
}

&sync_mysql_alert("2013-04-01");


sub get_mysql_meta {
    my ($dbh) = @_;
    my $sql = "select distinct host_name, ip, node, cluster from db_list where check_time > date_sub(now(), interval 1 day)";
    my $stmt = $dbh->prepare($sql);
    $stmt->execute();
    my $mymeta = $stmt->fetchall_hashref("ip");
    return $mymeta;
}

sub get_last_sync_time {
    my ($dbh) = @_;
    my $sql1 = "select max(date_in) from noti_log";
    my $sql2 = "select max(date_in) from alarm_log";
    my $stmt1 = $dbh->prepare($sql1);
    my $stmt2 = $dbh->prepare($sql2);
    $stmt1->execute();
    $stmt2->execute();
    my ($sync_time1) = $stmt1->fetchrow_array();
    my ($sync_time2) = $stmt2->fetchrow_array();
    print "[debug] $sync_time1, $sync_time2\n";
    return ($sync_time1, $sync_time2);
}

sub sync_mysql_alert_inc {
    my ($src_dbh, $dst_dbh, $last_sync_time, $mysql_meta) = @_;
    print "[info] sync mysql alert info $last_sync_time\n";

    my $sql_sel = <<EOD;
        select id, date_in, message, ip, service, hostgroup, noti_status, host_type
        from noti_log
        where date_in >= ?
        and host_type = 'db'
        and noti_status in ('WARNING', 'CRITICAL')
        and hostgroup like 'MYSQL%'
EOD


    my $sql_ins = <<EOD;
        insert ignore into noti_log(cluster, host, id, date_in, message, ip, service, hostgroup, noti_status, host_type)
        values(?,?,?,?,?,?,?,?,?,?)
EOD
    my $stmt_sel = $src_dbh->prepare($sql_sel);
    my $stmt_ins = $dst_dbh->prepare($sql_ins);

    $stmt_sel->execute($last_sync_time);
    my $rs = $stmt_sel->fetchall_arrayref();
    for my $r (@$rs) {
        my $ip = $r->[3];
        my $host = $mysql_meta->{$ip}{host_name};
        my $cluster = $mysql_meta->{$ip}{cluster};
        print "$ip, $cluster, $host\n";
        $cluster = "UNKNOWN" if (!defined($cluster));
        $host = "UNKNOWN" if (!defined($host));
        $stmt_ins->execute($cluster, $host,@$r);
    }
    
}

#create table alarm_log (
#    cluster varchar(30),
#    receive_type  varchar(255) ,
#    db_type       varchar(255) ,
#    host_type     varchar(255) ,
#    ip            varchar(255) ,
#    host          varchar(255) ,
#    wangwang      varchar(30),
#    date_in       datetime     ,
#    message       varchar(4000),
#    service       varchar(50)  ,
#    hostgroup     varchar(25)  ,
#    noti_status   varchar(10)  ,
#    id            int(11)     ,
#    primary key(id) 
#) engine=innodb;
#
sub sync_mysql_alert_by_person_inc {
    my ($src_dbh, $dst_dbh, $last_sync_time, $mysql_meta) = @_;
    print "[info] sync mysql alert info $last_sync_time\n";

    my $sql_sel = <<EOD;
        select receive_type, db_type, host_type, ip, date_in, message, service, hostgroup, noti_status, id, b.wangwang  from alarm_log_simple a,  Contact b where  date_in>= ? and host_type = 'db' and db_type='mysql' and b.contact_id = a.contact and noti_status in ('WARNING', 'CRITICAL')
EOD


    my $sql_ins = <<EOD;
        insert ignore into alarm_log(cluster, host, receive_type, db_type, host_type, ip, date_in, message, service, hostgroup, noti_status, id,wangwang  )
        values(?,?,?,?,?,?,?,?,?,?,?,?,?)
EOD
    my $stmt_sel = $src_dbh->prepare($sql_sel);
    my $stmt_ins = $dst_dbh->prepare($sql_ins);

    $stmt_sel->execute($last_sync_time);
    my $rs = $stmt_sel->fetchall_arrayref();
    for my $r (@$rs) {
        my $ip = $r->[3];
        my $cluster = $mysql_meta->{$ip}{cluster};
        my $host = $mysql_meta->{$ip}{host_name};
        print "$ip, $cluster, $host\n";
        $stmt_ins->execute($cluster, $host, @$r);
    }
}

#+-------------+---------------+------+-----+---------+----------------+
#| Field       | Type          | Null | Key | Default | Extra          |
#+-------------+---------------+------+-----+---------+----------------+
#| id          | int(11)       | NO   | PRI | NULL    | auto_increment |
#| cluster     | varchar(30)   | YES  |     | NULL    |                |
#| date_in     | datetime      | YES  | MUL | NULL    |                |
#| message     | varchar(4000) | YES  |     | NULL    |                |
#| host        | varchar(60)   | YES  |     | NULL    |                |
#| service     | varchar(50)   | YES  |     | NULL    |                |
#| hostgroup   | varchar(25)   | YES  |     | NULL    |                |
#| noti_status | varchar(10)   | YES  |     | NULL    |                |
#| host_type   | varchar(255)  | YES  |     | NULL    |                |
#| ip          | varchar(30)   | YES  |     | NULL    |                |


sub get_epoch {
    my $time_str = $_[0];
    #print "[debug] get_epoch $time_str\n";
    my ($date, $time) = split / /, $time_str;

    my ($year, $month, $day) = split /-/, $date;
    my ($hour, $minute, $second) = split /:/, $time;
    $time = timelocal($second, $minute, $hour, $day, $month - 1, $year-1900);
    #print "[debug] get_epoch $year, $month, $day, $hour, $minute, $second\n";
    #print "time: $time\n";
    return $time;
}

sub get_day_and_week {
    my $time;
    if(defined($opt{d})) {
        $time = &get_epoch($opt{d});
    } else {
        $time = time() - 3600 * 24;
    }
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($time);
    my $str_date = sprintf "%04d-%02d-%02d", $year+1900, $mon+1, $mday;
    print "$str_date, $wday\n";
    return ($str_date, $wday);
}
