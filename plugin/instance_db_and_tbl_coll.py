#!/usr/bin/python2.6
#coding=utf-8

import subprocess
import socket
import os,sys,datetime,MySQLdb
from numbers import Number
import comm


#hostname
f = open('/proc/sys/kernel/hostname', 'r')
HOST_NAME=f.readline().rstrip()
current_time=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

tianji_db=comm.MySQL()

SYS_SCHEMA=('information_schema','mysql','performance_schema','test')

def addp(s):
    return "'" + str(s) + "'"

def db(port,cursor,cluster):
    FIRST=False

    #previous schemas
    sql="select SCHEMA_NAME from meta_schema where host_name='%s' and port=%d" % (HOST_NAME, port)
    schemas=tianji_db.the_all(sql)
    old_schemas=list()
    if not schemas:
        FIRST=True
    else:
        for s in schemas:
            old_schemas.append(s['SCHEMA_NAME'])

    sql="select SCHEMA_NAME from SCHEMATA"
    cursor.execute(sql.encode('gbk'))
    rows=cursor.fetchall()
    if not rows:
        return False 
    
    dbs=list()
    for row in rows:
        if row['SCHEMA_NAME'] in SYS_SCHEMA:
            continue

        dbs.append(row['SCHEMA_NAME'])
        
        if row['SCHEMA_NAME'] in old_schemas:
            old_schemas.remove(row['SCHEMA_NAME'])
            continue
        else:
            #create DATABASE
            data={
                'host_name'     :HOST_NAME,
                'port'          :port,
                'SCHEMA_NAME'   :row['SCHEMA_NAME'],
                'GMT_CREATED'   :current_time,
            }
            print 'new schema,',data
            tianji_db.insert('meta_schema',data,UPDATE=True)

            if not FIRST:
                log={
                    'instance_name' :HOST_NAME + ':' + str(port),
                    'cluster'       :cluster,
                    'host_name'     :HOST_NAME,
                    'port'          :port,
                    'type'          :1,
                    'SCHEMA_NAME'   :row['SCHEMA_NAME'],
                    'check_time'    :current_time
                    }
                tianji_db.insert('meta_change_log',log)


    if len(old_schemas) > 0:

        sql="delete from meta_schema where host_name='%s' and port=%d and SCHEMA_NAME in (%s)" % (HOST_NAME, port , ','.join(map(addp,old_schemas)))
        tianji_db.execute(sql)

        sql="delete from meta_table where host_name='%s' and port=%d and SCHEMA_NAME in (%s)" % (HOST_NAME, port , ','.join(map(addp,old_schemas)))
        tianji_db.execute(sql)

        sql="delete from meta_column where host_name='%s' and port=%d and SCHEMA_NAME in (%s)" % (HOST_NAME, port , ','.join(map(addp,old_schemas)))
        tianji_db.execute(sql)

        #drop DATABASE
        for o in old_schemas:
            log={
                'instance_name' :HOST_NAME + ':' + str(port),
                'cluster'       :cluster,
                'host_name'     :HOST_NAME,
                'port'          :port,
                'type'          :2,
                'SCHEMA_NAME'   :o,
                'check_time'    :current_time
                }
            tianji_db.insert('meta_change_log',log)

    return dbs


def check_column(port,db,table,cursor,cluster):
    FIRST=False

    #old columns 
    sql="select COLUMN_NAME,COLUMN_TYPE from meta_column where host_name='%s' and port=%d and SCHEMA_NAME='%s' and TABLE_NAME='%s'" % (HOST_NAME, port,db,table)
    columns=tianji_db.the_all(sql)
    old_columns=dict()
    if not columns:
        FIRST=True
    else:
        for c in columns:
            old_columns[c['COLUMN_NAME']]=c

    sql="select COLUMN_NAME,COLUMN_TYPE from COLUMNS where TABLE_SCHEMA='%s' and TABLE_NAME='%s'" % (db,table)
    cursor.execute(sql)
    rows=cursor.fetchall()
    if not rows:
        return False 
    
    for row in rows:
        if row['COLUMN_NAME'] in old_columns.keys():
            if row['COLUMN_TYPE'] != old_columns[row['COLUMN_NAME']]['COLUMN_TYPE']:
                #COLUMN TYPE changed
                log={
                    'instance_name' :HOST_NAME + ':' + str(port),
                    'cluster'       :cluster,
                    'host_name'         :HOST_NAME,
                    'port'              :port,
                    'type'              :7,
                    'SCHEMA_NAME'       :db,
                    'TABLE_NAME'        :table,
                    'COLUMN_NAME'       :row['COLUMN_NAME'],
                    'COLUMN_TYPE'       :row['COLUMN_TYPE'],
                    'COLUMN_TYPE_OLD'   :old_columns[row['COLUMN_NAME']]['COLUMN_TYPE'],
                    'check_time'        :current_time
                    }
                tianji_db.insert('meta_change_log',log)
            del old_columns[row['COLUMN_NAME']]
        else:
            #add column
            data={
                'host_name'         :HOST_NAME,
                'port'              :port,
                'SCHEMA_NAME'       :db,
                'TABLE_NAME'        :table,
                'COLUMN_NAME'       :row['COLUMN_NAME'],
                'COLUMN_TYPE'       :row['COLUMN_TYPE'],
                'GMT_CREATED'       :current_time
            }
            print 'new column,',data
            tianji_db.insert('meta_column',data,UPDATE=True)

            if not FIRST:
                log={
                    'instance_name' :HOST_NAME + ':' + str(port),
                    'cluster'       :cluster,
                    'host_name'     :HOST_NAME,
                    'port'          :port,
                    'type'          :5,
                    'SCHEMA_NAME'   :db,
                    'TABLE_NAME'    :table,
                    'COLUMN_NAME'   :row['COLUMN_NAME'],
                    'COLUMN_TYPE'   :row['COLUMN_TYPE'],
                    'check_time'    :current_time
                    }
                tianji_db.insert('meta_change_log',log)


    if len(old_columns) > 0:

        sql="delete from meta_column where host_name='%s' and port=%d and SCHEMA_NAME='%s' and TABLE_NAME='%s' AND COLUMN_NAME in (%s)" % (HOST_NAME, port ,db,table, ','.join(map(addp,old_columns.keys())))
        tianji_db.execute(sql)

        #drop COLUMN
        for o in old_columns.keys():
            log={
                'instance_name' :HOST_NAME + ':' + str(port),
                'cluster'       :cluster,
                'host_name'     :HOST_NAME,
                'port'          :port,
                'type'          :6,
                'SCHEMA_NAME'   :db,
                'TABLE_NAME'    :table,
                'COLUMN_NAME'   :o,
                'COLUMN_TYPE'   :old_columns[o]['COLUMN_TYPE'],
                'check_time'    :current_time
                }
            tianji_db.insert('meta_change_log',log)


def table(port,dbs,cursor,cluster):
    if not dbs:
        return False
    
    for db in dbs:
        FIRST=False

        #previous tables
        sql="select TABLE_NAME,LAST_DDL_TIME from meta_table where host_name='%s' and port=%d and SCHEMA_NAME='%s'" % (HOST_NAME, port, db)
        tables=tianji_db.the_all(sql)
        old_tables=dict()
        if not tables:
            FIRST=True
        else:
            for s in tables:
                old_tables[s['TABLE_NAME']]=s

        #now tables
        sql="select TABLE_NAME,CREATE_TIME from TABLES where TABLE_SCHEMA='%s'" % db
        cursor.execute(sql)
        print sql
        rows=cursor.fetchall()
        if not rows:
            continue

        for row in rows:

            if row['TABLE_NAME'] in old_tables.keys():
                #table exists,check if column changed
                if row['CREATE_TIME'] != old_tables[row['TABLE_NAME']]['LAST_DDL_TIME']:
                    #see which column changed
                    check_column(port,db,row['TABLE_NAME'],cursor,cluster)

                del old_tables[row['TABLE_NAME']]

            else:
                #create table 
                tmp_t=current_time
                if row['CREATE_TIME']:
                    tmp_t=row['CREATE_TIME'].strftime("%Y-%m-%d %H:%M:%S")

                data={
                    'host_name'     :HOST_NAME,
                    'port'          :port,
                    'SCHEMA_NAME'   :db,
                    'TABLE_NAME'    :row['TABLE_NAME'],
                    'LAST_DDL_TIME' :tmp_t,
                    'GMT_CREATED'   :current_time
                }
                print 'new table,',data
                tianji_db.insert('meta_table',data,UPDATE=True)

                if not FIRST:
                    log={
                        'instance_name' :HOST_NAME + ':' + str(port),
                        'cluster'       :cluster,
                        'host_name'     :HOST_NAME,
                        'port'          :port,
                        'type'          :3,
                        'SCHEMA_NAME'   :db,
                        'TABLE_NAME'    :row['TABLE_NAME'],
                        'check_time'    :current_time
                        }
                    tianji_db.insert('meta_change_log',log)

                check_column(port,db,row['TABLE_NAME'],cursor,cluster)
            

        if len(old_tables) > 0:
            
            #drop table
            sql="delete from meta_table where host_name='%s' and port=%d and SCHEMA_NAME='%s' and TABLE_NAME in (%s)" % (HOST_NAME, port , db,','.join(map(addp,old_tables)))
            tianji_db.execute(sql)
            
            #drop column
            sql="delete from meta_column where host_name='%s' and port=%d and SCHEMA_NAME='%s' and TABLE_NAME in (%s)" % (HOST_NAME, port , db,','.join(map(addp,old_tables)))
            tianji_db.execute(sql)

            for o in old_tables:
                log={
                    'instance_name' :HOST_NAME + ':' + str(port),
                    'cluster'       :cluster,
                    'host_name'     :HOST_NAME,
                    'port'          :port,
                    'type'          :4,
                    'SCHEMA_NAME'   :db,
                    'TABLE_NAME'    :o,
                    'check_time'    :current_time
                    }
                tianji_db.insert('meta_change_log',log)


def check_server():
    args="ps -elf | grep mysqld| grep -v mysqld_safe|grep -v grep | awk '{ s = \"\"; for (i = 15; i <= NF; i++) s = s $i \" \"; print s }'"
    proc=subprocess.Popen(args,shell=True,stdout=subprocess.PIPE)
    while True:
        line=proc.stdout.readline().rstrip()
        if not line:
            break
        nfields=len(line.split()) - 1 
        inst=line.split(None,nfields)

        port=None
        if inst:
            for fi in inst:
                if fi.startswith('--port'):
                    _,port=fi.split('=')

        if port:
            port=int(port)
            conn = MySQLdb.connect(host='127.0.0.1', user='monitor', passwd='monitor', db='information_schema',port=port,charset="gbk")
            conn.autocommit(1)
            cursor = conn.cursor(MySQLdb.cursors.DictCursor)

            #get it's cluster name 
            sql="select cluster from db_list where instance_name='%s' limit 1" % (HOST_NAME + ':' + str(port))
            tianji=comm.MySQL()
            tmp_row=tianji.the_one(sql)
            cluster=''
            if tmp_row:
                cluster=tmp_row['cluster']

            dbs=db(port ,cursor,cluster)
            table(port,dbs ,cursor,cluster)
                            

if __name__ == '__main__':
    import random,time,os
    random.seed(os.getpid()+time.time())
    time.sleep( random.random()*600 )
    check_server()
