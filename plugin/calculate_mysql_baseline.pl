#!/home/oracle/dbaperl/bin/perl
use Time::Local;
#
# Purpose: gather mysql warn info
# rules: threads_running > 20, rt > 100ms, disk, qps > 10000, tps > 10000, slave delay > 100
#
# base line: qps, tps, ins, upd, del, sel, connections, threads_running
#
# 
#
#
#  create table mysql_baseline0 (
#      instance_name varchar(30),
#      hhmm varchar(4) comment 'hhmm',
#      check_time datetime,
#      sqps int,
#      qps int,
#      tps int,
#      ins int,
#      upd int,
#      del int,
#      sel int,
#      connections int,
#      threads_running int,
#      rt int,
#      load1 int,
#      cpu_user decimal(4,2),
#      cpu_wait decimal(4,2),
#      cpu_sys decimal(4,2),
#      net_recv int,
#      net_send int,
#      primary key(instance_name, hhmm)
#  ) engine=innodb;
#  
#  create table mysql_baseline1 like mysql_baseline0;
#  create table mysql_baseline2 like mysql_baseline0;
#  create table mysql_baseline3 like mysql_baseline0;
#  create table mysql_baseline4 like mysql_baseline0;
#  create table mysql_baseline5 like mysql_baseline0;
#  create table mysql_baseline6 like mysql_baseline0;
#  create table mysql_baseline like mysql_baseline0;

#config:
#    kpi, absolute_thres, relative_thres, 
#    mysql: qps, tps, sel,ins,upd,del,innodb_bp_reads, innodb_bp_read_requests,innodb_row_lock_waits,innodb_row_lock_time_avg,
#            threads_running, threads_connected,
#    rt_tcprstat: rt_avg

use DBI;
use Encode;
use Getopt::Long;

###########################################
sub print_usage () {
  my $text = <<EOF;
  
 NAME:
    tbsync

 Example:
    sync t1 to t2 in localhost:
        tbsync  --ds1=127.0.0.1:3306:user:pass --db1=test --tb1=t1 --tb2=t2

    sync t1 to t2 in localhost, only sync today's data
        tbsync  --ds1=127.0.0.1:3306:user:pass --db1=test --tb1=t1 --tb2=t2 \
                --filter="gmt_modified>date_sub(now(),interval 1 day)"

    sync t1 to t2 in host1 and host2
        tbsync  --ds1=host1:3306:user:pass --ds2=host2:port:user:pass --db1=test --tb1=t1 --tb2=t2 \
    

 FUNCTION:
    Table structure and table data compare and sync

 PARAMETER:
    most of the parameter is self explained
        --ds1
           ds1: ip:port:user:password
        --ds2
           ds2: ip:port:user:password
        --db1
           db1 name, *Required*
        --db2
           db2 name, if not supplied, default to db1
        --tb1
           table 1 name, if not supplied, compare all tables in db1(not implemented yet)
           support fuzzy match, if fuzzy, tb2 will be ignored
        --tb2
           table 2 name, if not supplied, same as tb1
        --match-table
            match table
        --ignore-table
            ignore-table
        --filter
            extra where cluase, only sync matched records
        --print
          only print diff, not apply, default
        --execute
          will apply diff(maybe will NOT implement this)
        -v|--verbose
            show more info
        -h|--help
            print help
        --sync-to-master(todo)
        --no-data
            only check table structure
        --recheck=n

EOF
 print STDERR $text;
 exit 0;
}

my %opt = (
);

GetOptions(\%opt,
    'd|day=s',           #day 2012-11-11
    'h|help',            # help
    'recheck=i'
) or print_usage();

if ( $opt{h} ) {
    &print_usage();
}

#conn to mysql db using DBI(dbd-mysql)
sub get_mysql_connection {
    my ($mysql_instance, $user, $pass, $db) = @_;
    my ($ip_addr, $port) = split /:/, $mysql_instance;
    my $str_conn = "dbi:mysql:host=$ip_addr;port=$port;db=$db";
    local $SIG{ALRM} = sub { die "Connect to MySQL $ip_addr:$port timed out\n" };
    alarm 20;
    my $mydb = DBI->connect(
        $str_conn,
        "$user", "$pass") or die "Connect to MySQL $ip_addr:$port error:". DBI->errstr;
    $mydb->{FetchHashKeyName} = 'NAME_lc';
    $mydb->{AutoCommit} = 1;
    $mydb->{RaiseError} = 1;
    $mydb->{PrintError} = 1;

    $mydb->do("set names gbk");
    alarm 0;
    return $mydb;
}

sub get_instance_list {
    my ($dbh) = @_;
    my $sql = "select instance_name, host_name from db_list where check_time > date_sub(now(), interval 12 hour)";
    my $stmt = $dbh->prepare($sql);
    $stmt->execute();
    my $rset = $stmt->fetchall_arrayref();
    return $rset;
}

sub calculate_baseline_1min {
    my ($dbh, $instance_name, $host_name, $start_time, $end_time, $h, $m, $week) = @_;
    my $sql_mysqlstat = <<EOD;
        select avg(sqps) sqps, avg(qps) qps, avg(tps) tps, avg(ins) ins, avg(upd) upd, avg(del) del, 
            avg(sel) sel, avg(threads_connected) conn, avg(threads_running) act
        from mysql_stat
        where instance_name = ?
        and check_time >= ?
        and check_time < ?
EOD
    my $sql_mysqlrt = <<EOD;
        select avg(rt_avg) rt_avg
        from rt_tcprstat
        where instance_name = ?
        and check_time >= ?
        and check_time < ?
EOD
    my $sql_hoststat = <<EOD;
        select avg(load1) load1, avg(cpu_usr) cpu_usr, avg(cpu_sys) cpu_sys, avg(cpu_wai) cpu_wai,
            avg(net_recv) net_recv, avg(net_send) net_send
        from linux_stat
        where host_name = ?
        and check_time >= ?
        and check_time < ?
EOD
    my $save_baseline = <<EOD;
        replace into mysql_baseline_$week(instance_name, hhmm, check_time, 
            sqps, qps, tps, ins, upd, del, 
            sel, connections, threads_running, 
            rt,
            load1, cpu_user, cpu_wait, cpu_sys, net_recv, net_send)
        values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
EOD
    #mysql_stat
    #print "$sql_mysqlstat\n$sql_mysqlrt\n$sql_hoststat\n$start_time\n$end_time\n";
    #print "$h # $m # $start_time # $end_time\n";
    my $stmt = $dbh->prepare($sql_mysqlstat);
    $stmt->execute($instance_name, $start_time, $end_time);
    my @r_mysqlstat = $stmt->fetchrow_array();

    #rt_tcprstat
    $stmt = $dbh->prepare($sql_mysqlrt);
    $stmt->execute($instance_name, $start_time, $end_time);
    my @r_mysqlrt = $stmt->fetchrow_array();

    #linux_stat
    $stmt = $dbh->prepare($sql_hoststat);
    $stmt->execute($host_name, $start_time, $end_time);
    my @r_hoststat = $stmt->fetchrow_array();

    $stmt = $dbh->prepare($save_baseline);
    my $hhmm = sprintf "%02d%02d", $h, $m;
    #print "$hhmm\n";
    #print "@r_mysqlstat # @r_mysqlrt # @r_hoststat\n";
    $stmt->execute($instance_name, $hhmm, $start_time, @r_mysqlstat, @r_mysqlrt, @r_hoststat);
}

#+-----------------+--------------+------+-----+---------+-------+
#| Field           | Type         | Null | Key | Default | Extra |
#+-----------------+--------------+------+-----+---------+-------+
#| instance_name   | varchar(30)  | NO   | PRI |         |       |
#| hhmm            | varchar(4)   | NO   | PRI |         |       |
#| check_time      | datetime     | YES  |     | NULL    |       |
#| sqps            | int(11)      | YES  |     | NULL    |       |
#| qps             | int(11)      | YES  |     | NULL    |       |
#| tps             | int(11)      | YES  |     | NULL    |       |
#| ins             | int(11)      | YES  |     | NULL    |       |
#| upd             | int(11)      | YES  |     | NULL    |       |
#| del             | int(11)      | YES  |     | NULL    |       |
#| sel             | int(11)      | YES  |     | NULL    |       |
#| connections     | int(11)      | YES  |     | NULL    |       |
#| threads_running | int(11)      | YES  |     | NULL    |       |
#| rt              | int(11)      | YES  |     | NULL    |       |
#| load1           | int(11)      | YES  |     | NULL    |       |
#| cpu_user        | decimal(4,2) | YES  |     | NULL    |       |
#| cpu_wait        | decimal(4,2) | YES  |     | NULL    |       |
#| cpu_sys         | decimal(4,2) | YES  |     | NULL    |       |
#| net_recv        | int(11)      | YES  |     | NULL    |       |
#| net_send        | int(11)      | YES  |     | NULL    |       |
#+-----------------+--------------+------+-----+---------+-------+

sub calculate_baseline {
    #my ($dbh, $instance_name, $host_name, $day) = @_;
    my $dbh = &get_mysql_connection("127.0.0.1:3306", "tianji", "monitor", "monitor");
    #my $instance_name = "dbtask.zue:3306";
    #my $host_name = "dbtask.zue";
    my $instance_list = &get_instance_list($dbh);
    
    my ($day, $week) = &get_day_and_week();
    
    for my $inst(@$instance_list) {
        my ($instance_name, $host_name) = ($inst->[0], $inst->[1]);
        print "$instance_name, $host_name\n";
        for (my $h = 0; $h < 24; $h++ ) {
            for(my $m = 0; $m < 60; $m ++) {
                my $t1 = sprintf "$day %02d:%02d:00", $h, $m;
                my $t2 = sprintf "$day %02d:%02d:59", $h, $m;
                &calculate_baseline_1min($dbh, $instance_name, $host_name, $t1, $t2, $h, $m, $week);
            }
        }
        $dbh->commit();
    }
    $dbh->disconnect();
}

#&calculate_baseline();

my @all_kpi  = qw (sqps  qps  tps  ins  upd  del  sel  connections  threads_running  rt  load1  cpu_user  cpu_wait  cpu_sys  net_recv  net_send);
 sub calculate_avg_baseline {
    my $dbh = &get_mysql_connection("127.0.0.1:3306", "tianji", "monitor", "monitor");
    my $instances = &get_instance_list($dbh);
    for my $instance (@$instances)  {
        my $instance_name = $instance->[0];

        my $sql_t = <<EOD;
            select
                concat(instance_name, ':', hhmm) as pt,
                sqps, qps, tps, ins, upd, del, 
                sel, connections, threads_running, 
                rt,
                load1, cpu_user, cpu_wait, cpu_sys, net_recv, net_send
            from mysql_baseline_%d
            where instance_name = ?
EOD

        my $sql_saveavg = <<EOD;
            replace into mysql_baseline(instance_name, hhmm, check_time, 
                sqps, qps, tps, ins, upd, del, 
                sel, connections, threads_running, 
                rt,
                load1, cpu_user, cpu_wait, cpu_sys, net_recv, net_send)
            values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
EOD
        my $stmt_save = $dbh->prepare($sql_saveavg);

        my @r;
        for (my $week = 0; $week < 7; $week ++) {
            my $sql = sprintf $sql_t, $week;
            my $stmt = $dbh->prepare($sql);
            $stmt->execute($instance_name);
            $r[$week] = $stmt->fetchall_hashref("pt");
        }
        my ($day, $dummy) = &get_day_and_week();
        for (my $h = 0; $h < 24; $h ++) {
            for (my $m = 0; $m < 60; $m ++) {
                my $k = sprintf "%s:%02d%02d", $instance_name, $h, $m;
                my @avg_vals;
                for my $kpi (@all_kpi) {
                    my $avg =($r[0]->{$k}{$kpi} +
                              $r[1]->{$k}{$kpi} +
                              $r[2]->{$k}{$kpi} +
                              $r[3]->{$k}{$kpi} +
                              $r[4]->{$k}{$kpi} +
                              $r[5]->{$k}{$kpi} +
                              $r[6]->{$k}{$kpi}) / 7; 

                    #print "$kpi $avg\n";
                    push @avg_vals, $avg;
                }
                my $hhmm = sprintf "%d%d", $h, $m;
                my $check_time = sprintf "%s %d:%d:00", $day, $h, $m;
                $stmt_save->execute($instance_name, $hhmm, $check_time, @avg_vals);
            }
        }
        $dbh->commit();
    }
    $dbh->disconnect();
}

sub main {
    #calculate last day's baseline
    &calculate_baseline();
    
    #update 7 day avg baseline
    &calculate_avg_baseline();
}

&main();

#utility routines
sub get_mysql_connection {
    my ($mysql_instance, $dbname, $user, $pass) = @_;
    my ($ip_addr, $port) = split /:/, $mysql_instance;
    my $db_str = "";
    if(defined($dbname)) {
       $db_str = ":database=$dbname";
    }
    my $str_conn = "dbi:mysql:host=$ip_addr$db_str;port=$port";
    print "[debug] get_mysql_connection $str_conn\n";
    local $SIG{ALRM} = sub { die "Connect to MySQL $ip_addr:$port timed out\n" };
    alarm 20;
    my $mydb = DBI->connect(
        $str_conn,
        "$user", "$pass") or die "Connect to MySQL $ip_addr:$port error:". DBI->errstr;
    $mydb->{FetchHashKeyName} = 'NAME_lc';
    $mydb->{AutoCommit} = 0;
    $mydb->{RaiseError} = 1;
    $mydb->{PrintError} = 1;

    $mydb->do("set names gbk");
    alarm 0;
    return $mydb;
}

sub get_epoch {
    my $time_str = $_[0];
    #print "[debug] get_epoch $time_str\n";
    my ($date, $time) = split / /, $time_str;

    my ($year, $month, $day) = split /-/, $date;
    my ($hour, $minute, $second) = split /:/, $time;
    $time = timelocal($second, $minute, $hour, $day, $month - 1, $year-1900);
    #print "[debug] get_epoch $year, $month, $day, $hour, $minute, $second\n";
    #print "time: $time\n";
    return $time;
}

sub get_day_and_week {
    my $time;
    if(defined($opt{d})) {
        $time = &get_epoch($opt{d});
    } else {
        $time = time() - 3600 * 24;
    }
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($time);
    my $str_date = sprintf "%04d-%02d-%02d", $year+1900, $mon+1, $mday;
    print "$str_date, $wday\n";
    return ($str_date, $wday);
}
