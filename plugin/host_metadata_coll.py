#!/usr/bin/python2.6
#coding=utf-8

import subprocess
import socket
import os,sys,inspect,datetime

import comm


infos=dict()

def insert_into_db(infos):
    if not infos:
        return False

    infos['gmt_created']=infos['gmt_modified']=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    db=comm.MySQL()
    db.insert('meta_host',infos,UPDATE=True)

def check_server():
    #hostname
    f = open('/proc/sys/kernel/hostname', 'r')
    infos['host_name']=f.readline().rstrip()

    #SN
    proc=subprocess.Popen(["dmidecode", "-t" ,"1"],stdout=subprocess.PIPE)
    while True:
        line=proc.stdout.readline()
        if not line:
            break

        line=line.strip()
        if line.startswith('Manufacturer'):
            manufacturer=line.split(':')
            infos['manufacturer']=manufacturer[1].strip()
        elif line.startswith('Product Name'):
            pn=line.split(':')
            infos['product_name']=pn[1].strip()
        elif line.startswith('Family'):
            family=line.split(':')
            infos['family']=family[1].strip()
        elif line.startswith('Serial Number'):
            sn=line.split(':')
            infos['sn']=sn[1].strip()

    #timezone
    proc=subprocess.Popen(['date','+%Z'],stdout=subprocess.PIPE)
    tz=proc.stdout.readline().strip()
    infos['timezone']=tz

    #numa
    proc=subprocess.Popen(['numactl','--hardware'],stdout=subprocess.PIPE)
    while True:
        line=proc.stdout.readline()
        if not line:
            break
        line=line.strip()
        if line.startswith('available'):
            _,nodes=line.split(':')
            nodes=nodes.strip()
            num,_=nodes.split(' ',1)
            infos['numa_nodes']=int(num)
    #ip
    #infos['ip']=socket.gethostbyname(infos['host_name'])

    #idc
    #infos['idc']=''

    #platform
    #f = open('/proc/sys/kernel/ostype', 'r')
    #infos['platform']=f.readline().rstrip()

    #release
    f = open('/etc/redhat-release', 'r')
    infos['release']=f.readline().rstrip()

    #kernel release
    #f = open('/proc/sys/kernel/osrelease', 'r')
    #infos['kernel']=f.readline().rstrip()

    #arch
    #proc=subprocess.Popen(["uname","-m"],stdout=subprocess.PIPE)
    #infos['arch']=proc.stdout.readline().rstrip()

    #threading
    proc=subprocess.Popen(["getconf","GNU_LIBPTHREAD_VERSION"],stdout=subprocess.PIPE)
    infos['threading']=proc.stdout.readline().rstrip()
    
    #agent version
    #infos['agent_version']=''

    #updays
    #proc=subprocess.Popen("uptime | awk '{print $3}' ",shell=True,stdout=subprocess.PIPE)
    #infos['updays']=proc.stdout.readline().rstrip()

    #compiler
    if os.path.exists("/lib64/libc.so.6"):
        proc=subprocess.Popen(["/lib64/libc.so.6 | grep 'Compiled by' | cut -c13-"],shell=True,stdout=subprocess.PIPE)
    else:
        proc=subprocess.Popen(["/lib/libc.so.6 | grep 'Compiled by' | cut -c13-"],shell=True,stdout=subprocess.PIPE)
    infos['compiler']=proc.stdout.readline().rstrip()

    #SELinux
    #proc=subprocess.Popen(["sestatus"],stdout=subprocess.PIPE)
    #sestatus=proc.stdout.readline().rstrip().split(":")
    #if sestatus and sestatus[0].startswith('SELinux'):
    #    infos['selinux']=sestatus[1].strip()

    #cpu physical
    proc=subprocess.Popen(["cat /proc/cpuinfo|grep 'physical id'|sort -u|wc -l"],shell=True,stdout=subprocess.PIPE)
    infos['cpu_physical']=int(proc.stdout.readline().strip())

    #cpu cores
    proc=subprocess.Popen(["cat /proc/cpuinfo|grep 'cpu cores' | head -n 1 | cut -d: -f2"],shell=True,stdout=subprocess.PIPE)
    infos['cpu_cores']=int(proc.stdout.readline().strip())

    #cpu virtual 
    proc=subprocess.Popen(["cat /proc/cpuinfo | grep -c ^processor"],shell=True,stdout=subprocess.PIPE)
    infos['cpu_virtual']=int(proc.stdout.readline().strip())

    #cpu speed 
    proc=subprocess.Popen(["cat /proc/cpuinfo |awk -F: '/cpu MHz/{print $2}' | sort | uniq -c"],shell=True,stdout=subprocess.PIPE)
    speed=proc.stdout.readline().strip().split()
    infos['cpu_speed']="%sx%s" % (speed[0] , speed[1])

    #cpu model 
    proc=subprocess.Popen(["cat /proc/cpuinfo| awk -F: '/model name/{print $2}'  | sort | uniq -c"],shell=True,stdout=subprocess.PIPE)
    model=proc.stdout.readline().strip().split(' ',1)
    infos['cpu_model']="%sx%s" % (model[0] , model[1])

    #cpu cache 
    proc=subprocess.Popen(["cat /proc/cpuinfo| awk -F: '/cache size/{print $2}' | sort | uniq -c"],shell=True,stdout=subprocess.PIPE)
    cache=proc.stdout.readline().strip().split()
    infos['cpu_cache']="%sx%s" % (cache[0] , cache[1])

    #memory
    proc=subprocess.Popen(["free -m |awk '/Mem:/{print $2}'"],shell=True,stdout=subprocess.PIPE)
    mem=proc.stdout.readline().strip().split()
    infos['memory']=mem[0]

    #swappiness
    f = open('/proc/sys/vm/swappiness', 'r')
    infos['swappiness']=f.readline().rstrip()

    #disk info
    #proc=subprocess.Popen(["df -lhTP | awk  '{print $1,$2,$3,$7}' OFS=','"],shell=True,stdout=subprocess.PIPE)
    #disks=list()
    #skip_first=True
    #while True:
    #    line=proc.stdout.readline()
    #    if skip_first:
    #        skip_first=False
    #        continue
    #    if line != '':
    #        a=line.rstrip()
    #        if a:
    #            disks.append(a)
    #    else:
    #        break
    #if disks:
    #    infos['disks']='|'.join(disks)
    
    #disk scheduler and queue size
    disk_schedulers=list()
    proc=subprocess.Popen(["ls /sys/block/ | grep -v -e ram -e loop -e 'fd[0-9]' | xargs echo"],shell=True,stdout=subprocess.PIPE)
    disks=proc.stdout.readline().strip().split()
    for d in disks:
        sche_info=[d,]

        if os.path.exists("/sys/block/%s/queue/scheduler" % d):
            proc=subprocess.Popen(["cat /sys/block/"+d+"/queue/scheduler | grep -o '\[.*\]'"],shell=True,stdout=subprocess.PIPE)
            sche=proc.stdout.readline().strip()
            sche_info.append(sche)

            f = open("/sys/block/"+d+"/queue/nr_requests", 'r')
            sche_info.append(f.readline().strip())

            sche_info=",".join(sche_info)

            disk_schedulers.append(sche_info)
    if disk_schedulers:
        infos['disk_schedulers']="|".join(disk_schedulers)

    #raid info

    #network info
    #proc=subprocess.Popen(["lspci |grep -i ethernet | cut -d: -f3"],shell=True,stdout=subprocess.PIPE)
    #eths=list()
    #while True:
    #    line=proc.stdout.readline()
    #    if not line:
    #        break

    #    eths.append(line.strip())
    #if eths:
    #    infos['eths']="|".join(eths)

    #for i in infos.keys():
    #    print i ,':',infos[i]
    insert_into_db(infos)

if __name__ == '__main__':
    import random,time,os
    random.seed(os.getpid()+time.time())
    time.sleep( random.random()*6 )
    check_server()

