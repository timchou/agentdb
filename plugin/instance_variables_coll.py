#!/usr/bin/python2.6
#coding=utf-8

import subprocess
import socket
import os,sys,datetime,MySQLdb
from numbers import Number
import comm


#hostname
f = open('/proc/sys/kernel/hostname', 'r')
HOST_NAME=f.readline().rstrip()

tianji_db=comm.MySQL()

def process_row(row,port,instance_name,cluster):
    if not row:
        return None

    vals=list()
    vals.append(HOST_NAME)
    vals.append(instance_name)
    vals.append(cluster)
    vals.append(port)
    vals.append(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    vals.append(row['Variable_name'])
    vals.append(row['Value'])
    vals.append(row['cnf_Value'])

    def packData(value):
        if value is None:
            return '"null"'
        if isinstance(value,str) or isinstance(value,unicode):
            #value=self.escape(value)
            return '"' + value + '"' 
        elif isinstance(value,int):
            return str(value)
        elif isinstance(value,Number):
            return str(value)
        elif isinstance(value,datetime):
            return str(value)

    s="(" + ",".join(map(packData,vals)) + ")" 
    return s

def check_server():
    args="ps -elf | grep mysqld| grep -v mysqld_safe|grep -v grep | awk '{ s = \"\"; for (i = 15; i <= NF; i++) s = s $i \" \"; print s }'"
    proc=subprocess.Popen(args,shell=True,stdout=subprocess.PIPE)
    instances=list()
    while True:
        line=proc.stdout.readline().rstrip()
        if not line:
            break
        nfields=len(line.split()) - 1 
        inst=line.split(None,nfields)
        
        port=None
        if inst:
            for fi in inst:
                if fi.startswith('--port'):
                    _,port=fi.split('=')

        if port:
            instance_name=HOST_NAME + ':' + str(port)
            sql="select cluster from db_list where instance_name='%s' limit 1" % instance_name
            tmp_r=tianji_db.the_one(sql)
            cluster=''
            if tmp_r:
                cluster=tmp_r['cluster']

            #my.cnf
            mycnf="/data/mysql" + str(port) + "/my.cnf"
            if not os.path.exists(mycnf):
                continue
            f=open(mycnf,'r')
            if not f:
                continue

            cnf_rows=list()
            while True:
                l=f.readline()
                if not l:
                    break
                if l.strip() == "":
                    continue
                if l.startswith('['):
                    continue
                if l.startswith('#'):
                    continue

                k=None
                v=None
                try:
                    k,v=l.split('=')
                except ValueError:
                    k=l
                    v='ON'
                cnf_rows.append({'Variable_name':k.strip() , 'cnf_Value':v.strip()})            

            #show varibales
            conn = MySQLdb.connect(host='127.0.0.1', user='monitor', passwd='monitor', db='information_schema',port=int(port),charset="gbk")
            conn.autocommit(1)
            cursor = conn.cursor(MySQLdb.cursors.DictCursor)
            
            sql="show variables"
            cursor.execute(sql.encode('gbk'))
            rows=cursor.fetchall()
            if not rows:
                break

            for row in rows:
                row['cnf_Value']=''
                for cnf_row in cnf_rows[:]:
                    if cnf_row['Variable_name'] == row['Variable_name']:
                        row['cnf_Value']=cnf_row['cnf_Value']
                        cnf_rows.remove(cnf_row)
            
            def addp(value):
                return '`'+value+'`'

            cols=rows[0].keys()
            values=[process_row(row,int(port),instance_name,cluster) for row in rows]
            if values:
                sql="INSERT INTO instance_all_variables(host_name,instance_name,cluster,port,check_time,Variable_name,Value,cnf_Value) values%s ON DUPLICATE KEY UPDATE `pre_Value`=Value,pre_check_time=check_time,`check_time`=VALUES(`check_time`),`Value`=VALUES(`Value`),`cnf_Value`=VALUES(`cnf_Value`),instance_name=VALUES(instance_name),cluster=VALUES(cluster)" % (",".join(values) )
                #print sql
                tianji_db.execute(sql)

if __name__ == '__main__':
    import random,time,os
    random.seed(os.getpid()+time.time())
    time.sleep( random.random()*6 )
    check_server()
