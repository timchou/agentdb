#!/bin/bash

AGENTDB_DIR='/u01/agentdb'

export PYTHONPATH=$PYTHONPATH:$AGENTDB_DIR/:$AGENTDB_DIR/libs/:$AGENTDB_DIR/libs/distribute-0.6.36-py2.6.egg/:$AGENTDB_DIR/dbagent/:$AGENTDB_DIR/plugin/
export MYSQL_HOME=/data/mysql3306
