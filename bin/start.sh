#!/bin/bash


source /u01/agentdb/bin/export.sh

$AGENTDB_DIR/bin/gunicorn -w 1 -b 0.0.0.0:8046 dbagent:app --access-logfile $AGENTDB_DIR/logs/gunicorn.access --error-logfile $AGENTDB_DIR/logs/gunicorn.error --daemon

#$DIR/bin/gunicorn -w 1 -b 0.0.0.0:8000 dbagent:app
